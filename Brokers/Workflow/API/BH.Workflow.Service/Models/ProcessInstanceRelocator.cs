﻿using BH.ScalableServices.Brokers.Workflow.Shared.Interfaces;
using BH.ScalableServices.Brokers.Workflow.Shared.Models;
using System.IO;
using System.Reflection;
using SystemWrapper.IO;

namespace BH.ScalableServices.Brokers.Workflow.Models
{
    public class ProcessInstanceRelocator : IRelocator<IWorkflowProcessInstance>
    {
        private WorkflowSettings Settings { get; set; }
        public IFileWrap FileIO { get; set; }

        public IDirectoryWrap DirIO { get; set; }

        public ProcessInstanceRelocator(IFileWrap fileWrap, IDirectoryWrap dirWrap, WorkflowSettings settings)
        {
            FileIO = fileWrap;
            DirIO = dirWrap;
            Settings = settings;
            Settings.LoadSettings(Path.GetFileName(Assembly.GetEntryAssembly().CodeBase));
        }


        public void Relocate<T>(string oldPath, string newPath)
        {
            oldPath = Path.Combine(Settings.ProcessBundlesLibraryLocation, oldPath);
            newPath = Path.Combine(Settings.ProcessBundlesLibraryLocation, newPath);

            if (DirIO.Exists(oldPath))
                DirIO.Move(oldPath, newPath);
        }
    }
}
