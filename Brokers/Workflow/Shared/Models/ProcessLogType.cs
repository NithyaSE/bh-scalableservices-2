﻿namespace BH.ScalableServices.Brokers.Workflow.Shared.Models
{
    public enum ProcessLogType
    {
        Info,
        Error,
        Warning
    }
}
