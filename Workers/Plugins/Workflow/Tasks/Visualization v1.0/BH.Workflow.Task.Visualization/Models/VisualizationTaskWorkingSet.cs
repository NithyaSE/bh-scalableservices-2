﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BH.ScalableServices.Workers.Plugins.Workflow.Tasks.Visualization.v1_0.Models
{
    public class VisualizationTaskWorkingSet
    {
        [JsonProperty("widget_instance")]
        public string WidgetInstance { get; set; }
        [JsonProperty("datasource_instance")]
        public string DatasourceInstance { get; set; }
        [JsonProperty("repository")]
        public JObject Repository { get; set; }
        [JsonProperty("file_properties")]
        public JObject FileProperties { get; set; }
    }
}
