﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using BH.Excel.Automation;
using BH.ScalableServices.Workers.Plugins.Workflow.Shared.Interfaces;
using BH.ScalableServices.Workers.Plugins.Workflow.Shared.Models;
using BH.ScalableServices.Workers.Plugins.Workflow.Tasks.ExcelWithParameters.v2_0.Models;
using BH.ScalableServices.Workers.Shared.Helpers;
using Shared.SerilogsLogging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Shared.Time;
namespace BH.ScalableServices.Workers.Plugins.Workflow.Tasks.ExcelWithParameters.v2_0
{
    public class ExcelWithParamsTask : WorkflowTask
    {
        private IExcelAPI ExcelApiLib { get; set; }
        private string tempFolderPath = "temp";
        public ExcelWithParamsTask(IExcelAPI excelAPI)
        {
            ExcelApiLib = excelAPI;
        }
        public ExcelWithParamsTask()
        {
            ExcelApiLib = new ExcelAPI();
        }
       public override IRunTaskReturn RunTask(JObject workingSet)
       {
           var excelWorkingSet = workingSet.ToObject<ExcelTaskWorkingSet>();
           Console.WriteLine("Running Excel Task! The name is: " + SystemTaskVariables.WorkflowTaskInstanceName);
           Serilog.Log.Logger.AddMethodName().Information("Summary='Running Excel Task '{@taskname}''", SystemTaskVariables.WorkflowTaskInstanceName);
           var macroName = excelWorkingSet.MacroName;
           var fileName = new FileInfo(excelWorkingSet.FileName).FullName;
           var isVisible = excelWorkingSet.IsVisible;
           var timeout = excelWorkingSet.Timeout;
           var timeoutInSeconds = GetTimeoutInSeconds(timeout);
           Serilog.Log.Logger.AddMethodName().Information("Summary='Parameters of Excel task  '{@taskname}'' Details='File Name - '{@fileName}' Macro name - '{@macroname}' Timeout Provided - '{@timeoutProvided}' Timeout In Secs - '{@timeoutInSec}' '"
                , SystemTaskVariables.WorkflowTaskInstanceName, macroName, fileName, timeout, timeoutInSeconds);
            LogEntryWriter.LogMessage($"Excel task '{SystemTaskVariables.WorkflowTaskInstanceName}' scheduled to timeout in '{timeoutInSeconds}' seconds",SystemTaskVariables.ProcessRunId);
            Console.WriteLine($"Excel task '{SystemTaskVariables.WorkflowTaskInstanceName}' scheduled to timeout in '{timeoutInSeconds}' seconds");
           var excelParameters = excelWorkingSet.ExcelParameters;
           var parameters = new Scripting.Dictionary();
           foreach (var param in excelParameters)
           {
                parameters.Add(param.Key,param.Value);
           }
           TryRunningExcel( fileName, timeoutInSeconds, macroName, parameters,isVisible, workingSet);
           return new FinishRunTaskReturn()
           {
               WorkingSet = workingSet
           };
       }
        private void TryRunningExcel(string fileName, int timeoutInSeconds, string macroName,Scripting.Dictionary parameters, bool isVisible,JObject workingSet)
        {
            var attemptNo = 0;
            var totalAttempts =3;
            string tempFileLocation = null;
            Scripting.Dictionary excelResponse;
            while (attemptNo++ <= totalAttempts)
            {
                Serilog.Log.Logger.AddMethodName()
                    .Debug("Summary='Trying to run the excel for attempt {attemp}'", attemptNo);
                try
                {
                    if (!Directory.Exists(tempFolderPath))
                        Directory.CreateDirectory(tempFolderPath);
                    else
                        DeleteFilesFromTempFolder();
                    var uniqueFileName = $@"{Guid.NewGuid()}.tmp";
                    tempFileLocation = Path.Combine(tempFolderPath, uniqueFileName);
                    tempFileLocation = Path.GetFullPath(tempFileLocation);
                    Serilog.Log.Logger.AddMethodName()
                        .Debug("Summary='Copying file '{@filename}' to temp location '{@tempfileName}''", fileName,
                            tempFileLocation);
                    File.Copy(fileName, tempFileLocation, false);
                    if (timeoutInSeconds != 0)
                        excelResponse = CallExcelMacroWithParameters(tempFileLocation, macroName, parameters, isVisible,
                            timeoutInSeconds);
                    else
                        excelResponse =
                            CallExcelMacroWithParameters(tempFileLocation, macroName, parameters, isVisible);
                    if (excelResponse == null) return;
                    try
                    {
                        var keys = excelResponse.Keys();
                        string keysJson = JsonConvert.SerializeObject(keys);
                        var keysList = JsonConvert.DeserializeObject<List<string>>(keysJson);
                        foreach (var key in keysList)
                        {
                         workingSet[key] = excelResponse.get_Item(key);
                        }
                    }
                    catch (Exception exp)
                    {
                        // if exception occurs in the code trying to access excelResponse then retry
                        Serilog.Log.Logger.AddMethodName()
                            .Error("Summary='Exception occured while readings the excelResponse in attemp {attempt}.' Details='{@exp}'",
                                attemptNo,exp);
                        continue;
                    }
                    break;
                }
                finally
                {
                    // Call ExcelApi to kill the process
                    ExcelApiLib.TryKillingExcelProcess();
                    // try to close file
                    try
                    {
                        if (!string.IsNullOrWhiteSpace(tempFileLocation) && File.Exists(tempFileLocation))
                        {
                            Serilog.Log.Logger.AddMethodName()
                                .Debug("Summary='Deleting temp file '{tempfile}''", tempFileLocation);
                            File.Delete(tempFileLocation);
                            Serilog.Log.Logger.AddMethodName()
                                .Debug("Summary='Deleted temp file '{tempfile}''", tempFileLocation);
                        }
                    }
                    catch (Exception exp)
                    {
                        Serilog.Log.Logger.AddMethodName()
                            .Warning("Summary='Failed to delete temp file '{tempfile}'' - {@Err}", tempFileLocation,
                                exp);
                    }
                }
            }
        }
        private void DeleteFilesFromTempFolder()
        {
            // Clean the temp folder
            Serilog.Log.Logger.AddMethodName().Debug("Summary='Trying to cleanup temp folder {@tempfolder}'", tempFolderPath);
            foreach (var filePath in Directory.EnumerateFiles(tempFolderPath))
            {
                try
                {
                    Serilog.Log.Logger.AddMethodName().Debug("Summary='Deleting temp file '{@filePath}'", filePath);
                    File.Delete(filePath);
                }
                catch (Exception exp)
                {
                    Serilog.Log.Logger.AddMethodName().Debug("Summary='Failed to delete temp file '{@filePath} - {@err}'", filePath, exp);
                }
            }
        }
        private int GetTimeoutInSeconds(string timeout)
        {
            Regex regex = new Regex("^P(?!$)(\\d+Y)?(\\d+M)?(\\d+W)?(\\d+D)?(T(?=\\d+[HMS])(\\d+H)?(\\d+M)?(\\d+S)?)?$");
            Match match = regex.Match(timeout);
            if (!match.Success)
            {
                throw new Exception($"Invalid format of check interval {timeout}. Only ISO interval format accepted");
            }
            int months = 0;
            int seconds = 0;
            int monthSec = 0;
            Time.ParseInterval(timeout, out months, out seconds);
            // get n months from current date and convert to seconds
            if (months != 0)
                monthSec = (DateTime.UtcNow - DateTime.UtcNow.AddMonths(months)).Seconds;
            return monthSec + seconds;
        }
        /// <summary>
        /// Calls an excel macro from C# using COM.  Allows 2 scripting dictionaries to be passed into the excel macro.  Returns a scripting dictionary if Excel macro returned one
        /// </summary>
        /// <param name="fileName">Full path to the excel file</param>
        /// <param name="macroName">name of the macro to call.  Macros on sheets will not pass back a dictionary</param>
        /// <param name="parameters">first dictionary excel macro accepts</param>
        /// <param name="parameters2">second dictionary excel macro accepts</param>
        /// <param name="isVisible">whether the excel file needs to be visible or not</param>
        /// <returns></returns>
        public Scripting.Dictionary CallExcelMacroWithParameters(string fileName, string macroName, Scripting.Dictionary parameters, bool isVisible, int timeout = 900)
        {
            using (ExcelApiLib)
            {
                ExcelApiLib.SetIsVisible(isVisible);
                //hard coded to not save the workbook by default = may need to make configurable in the future
                return ExcelApiLib.CallExcelMacroWithDictionary(fileName, macroName, parameters,
                    timeoutSeconds: timeout, save: false);
            }
        }
    }
}
