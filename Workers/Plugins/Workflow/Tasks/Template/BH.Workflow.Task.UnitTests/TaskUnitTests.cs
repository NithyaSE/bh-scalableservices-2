﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using BH.Shared.BloodhoundDataStore;
using BH.Shared.Server.Models;
using BH.Workflow.Task.Template;
using BHX.Workflow.Shared.Models.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;

namespace BH.Workflow.Task.UnitTests
{


    [TestClass]
    public class TaskUnitTests
    {
        [TestMethod]
        
        public void Check_Template_Task()
        {
            var mockJsonInstance = File.ReadAllText("MockTaskInstances\\mocktaskinstance1.json");

            var fakeBloodhoundLogic = new BH.FluffyBloodhound.FakeBusinessLogic
            {
                FakedBloodhoundDataResult = CreateFakeBloodhoundDataObject()
            };
            
            var task = new TemplateTask(fakeBloodhoundLogic)
            {
                BloodhoundTaskInstance = JsonConvert.DeserializeObject<BloodhoundTaskInstance>(mockJsonInstance)
            };

            var taskTest = new BH.FluffyBloodhound.Workflow.TestHarness.WorkflowTaskTester(task);
            taskTest.RegisterForOutputs(new List<string>
            {
                "Results"
            });


            taskTest.RunTask();


            var results = (string)taskTest.Outputs["Results"];
            var postedData = fakeBloodhoundLogic.FakePostedBloodhoundData;


            Assert.IsTrue(results == "This can be used in another process later." &&
                          postedData.Points.First().Readings.Count == 10 && 
                          postedData.Points.First().Readings[0].Value == "100" &&
                          postedData.Points.First().Readings[1].Value == "200" &&
                          postedData.Points.First().Readings[2].Value == "300" &&
                          postedData.Points.First().Readings[3].Value == "400" &&
                          postedData.Points.First().Readings[4].Value == "500" &&
                          postedData.Points.First().Readings[5].Value == "600" &&
                          postedData.Points.First().Readings[6].Value == "700" &&
                          postedData.Points.First().Readings[7].Value == "800" &&
                          postedData.Points.First().Readings[8].Value == "900" &&
                          postedData.Points.First().Readings[9].Value == "1000");



        }
        


        private BloodhoundData CreateFakeBloodhoundDataObject()
        {
            var readingsCollection = new BloodhoundReadingCollection();

            var fromDt = DateTime.UtcNow.AddHours(-10);
            var value = 100;

            //2 1/2 hours of cumulative data for testing
            for (var i = 1; i <= 10; i++)
            {
                readingsCollection.Add(new BloodhoundReading
                {
                    Timestamp = fromDt.AddMinutes(15 * i),
                    Value = value.ToString()
                });

                value += 100;
            }
            
            return new BloodhoundData
            {
                CustomerName = "FakeCustomer",
                
                Points = new BloodhoundPointCollection
                {
                    new BloodhoundPoint
                    {
                        FullName = "FakeFullName",
                        Tags = new BloodhoundTagCollection
                        {
                            new BloodhoundTag
                            {
                                Name = "TestTag",
                                Category = "TestCat"
                            }
                        },
                        Readings = readingsCollection
                    }
                }
            };
        }

    }





}
