﻿using System;
using System.Configuration;
using System.IO;
using SystemWrapper.IO;
using Amazon.SQS.Model;
using Shared.PluginRouter;
using Newtonsoft.Json;
using Serilog;
using BH.ScalableServices.Brokers.Shared.Models;
using BH.ScalableServices.Brokers.Workflow.Shared.Models;
using BH.ScalableServices.Brokers.Workflow.Shared.Interfaces;
using BH.ScalableServices.Brokers.Workflow.Shared.Respositories;
using BH.ScalableServices.Brokers.Workflow.SDK;
using BH.ScalableServices.Workers.Shared.Helpers;
using BH.ScalableServices.Workers.Shared.Interfaces;
using Shared.SerilogsLogging;
using Nancy.TinyIoc;
using Newtonsoft.Json.Linq;

namespace BH.ScalableServices.Workers.Plugins.Workflow
{
    /// <summary>
    /// Plugin for Worflow worker
    /// </summary>
    public class WorkflowWorkerPlugin : IWorkerPlugin
    {
        private static string AssemblyFullPath;
        public WorkflowWorkerPlugin()
        {
            var codeBase = typeof(WorkflowWorkerPlugin).Assembly.CodeBase;
            var uri = new UriBuilder(codeBase);
            AssemblyFullPath = Uri.UnescapeDataString(uri.Path);
        }
        public void ProcessMessage(ScalableServiceRunMessage<JToken> wfsReq) //  change from message to json string?
        {
            var runId = wfsReq?.RunId;
            StatusUpdater.ServiceWebClient = new WorkflowServiceClient();
            LogEntryWriter.ServiceWebClient = StatusUpdater.ServiceWebClient;
            RunProcess(runId);
        }
        private static void RunProcess(string runId)
        {
            var container = Bootstrap();
            var processRunsRepo = container.Resolve<IProcessRunsRepository>();
            // Use the repository to get the process instance from process runner 
            var workflowProcessInstance = processRunsRepo.GetProcessInstanceFromRunId(runId);
            workflowProcessInstance.RunId = runId;
            var workflowProcessRunner = container.Resolve<WorkflowRunner>();
            workflowProcessRunner.WorkflowProcessInstance = workflowProcessInstance;
            workflowProcessRunner.RunProcess();
        }
        protected static TinyIoCContainer Bootstrap()
        {
            var container = TinyIoCContainer.Current;
            container.Register<IWorkflowServiceClient, WorkflowServiceClient>();
            container.Register<IPluginRouter, PluginRouter>();
            container.Register<IFileWrap, FileWrap>();
            container.Register<IDirectoryWrap, DirectoryWrap>();
            container.Register<IWorkflowProcessInstance, WorkflowProcessInstance>();
            container.Register<IWorkflowTaskInstance, WorkflowTaskInstance>();
            var settings = new WorkflowSettings();
            settings.LoadSettings(AssemblyFullPath);
            Setup(settings);
            var config = ConfigurationManager.OpenExeConfiguration(AssemblyFullPath);
            Log.Logger = new LoggerConfiguration().ReadFrom.AppSettings(null,config.FilePath).CreateLogger();
            container.Register(settings);
            container.Register<IProcessRunsRepository, ProcessRunsRepository>();
            container.Register<WorkflowRunner>();
            return container;
        }
        protected static void Setup(WorkflowSettings settings)
        {
            //this was added to get away from 2 different config files
            WorkflowRunner.TasksLocation = settings.TasksLocation;
            JsonConvert.DefaultSettings = () => new JsonSerializerSettings
            {
                DateParseHandling = DateParseHandling.None
            };
        }
    }
}
