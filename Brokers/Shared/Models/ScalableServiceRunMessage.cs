﻿using System;
using BH.ScalableServices.Brokers.Shared.Enums;
using Newtonsoft.Json;

namespace BH.ScalableServices.Brokers.Shared.Models
{
    public class ScalableServiceRunMessage<T>
    {
        [JsonProperty("run_id")]
        public string RunId { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("message")]
        public T Message { get; set; }
        [JsonProperty("worker_type")]
        public WorkerType WorkerType { get; set; }

    }
}
