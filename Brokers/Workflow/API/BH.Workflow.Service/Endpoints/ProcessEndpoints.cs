﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using BH.ScalableServices.Brokers.APIBase.Interfaces;
using BH.ScalableServices.Brokers.Workflow.Models;
using BH.ScalableServices.Brokers.Workflow.Shared.Interfaces;
using BH.ScalableServices.Brokers.Workflow.Shared.Models;
using Nancy;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ResponseHandlerNancyModule = BH.ScalableServices.Brokers.Workflow.Handlers.ResponseHandlerNancyModule;

namespace BH.ScalableServices.Brokers.Workflow.Endpoints
{
    /// <summary>
    /// 
    /// </summary>
    public class ProcessEndpoints : ResponseHandlerNancyModule
    {
        private string routeVersion = "v2.0";
        public WorkflowSettings Settings { get; set; }
        public IProcessSchemaRepository ProcessSchemaRepo { get; set; }
        public ITaskSchemaRepository TaskSchemaRepo { get; set; }

        public ITaskInstanceRepository TaskInstanceRepo { get; set; }
        public IProcessInstanceRepository ProcessInstanceRepo { get; set; }
        public IRunDetailsRepository RunRepo { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public ProcessEndpoints(IRunDetailsRepository runRepo, IProcessInstanceRepository piRepo, IProcessSchemaRepository processSchemaRepo, ITaskSchemaRepository taskSchemaRepo, ITaskInstanceRepository taskInstancesRepo,  WorkflowSettings settings)
        {
            Settings = settings;
            RunRepo = runRepo;
            ProcessInstanceRepo = piRepo;
            ProcessSchemaRepo = processSchemaRepo;
            TaskInstanceRepo = taskInstancesRepo;
            TaskSchemaRepo = taskSchemaRepo;
            Settings.LoadSettings(Path.GetFileName(Assembly.GetEntryAssembly().CodeBase));

            Get[routeVersion + "/processes"] = parameters =>
            {
                var processNames = GetAllProcessNames();
                return Response.AsJson(processNames).WithStatusCode(HttpStatusCode.OK);
            };

            Get[routeVersion + "/process/{name}"] = parameters =>
            {
                var processDefinition = (ProcessDefinition) GetProcessByName(parameters.name);
                return Response.AsJson(processDefinition).WithStatusCode(HttpStatusCode.OK);
            };

            Get[routeVersion + "/process/status/{run_id}"] = parameters =>
            {
                var status = (ProcessStatus) GetProcessStatusByRunId(parameters.run_id);
                if (status != null)
                {
                    return Response.AsJson(status).WithStatusCode(HttpStatusCode.OK);
                }

                return HttpStatusCode.NotFound;
            };

            Post[routeVersion + "/process/delete/{name}"] = parameters =>
            {
                var processInstanceName = parameters.name.ToString();

                ProcessInstanceRepo.Remove(processInstanceName);

                return HttpStatusCode.OK;
            };

            Post[routeVersion + "/process/{name}"] = parameters =>
            {
                string body;
                using (var reader = new StreamReader(Request.Body))
                {
                    body = reader.ReadToEnd();
                }
                var processDefinition = JsonConvert.DeserializeObject<ProcessDefinition>(body);
                var processInstance = processDefinition.ProcessInstance.ToObject<WorkflowProcessInstance>();

                var mainPath = Settings.ProcessBundlesLibraryLocation;
                var oldProcessPath = mainPath + "\\" + parameters.name;
                var processPath = mainPath + "\\" + processInstance.Name;

                PostProcess(processPath, oldProcessPath, processDefinition);

                return HttpStatusCode.OK;
            };

            Post[routeVersion + "process/status/{run_id}"] = parameters =>
            {
                string body;
                using (var reader = new StreamReader(Request.Body))
                {
                    body = reader.ReadToEnd();
                }
                var processStatus = JsonConvert.DeserializeObject<ProcessStatus>(body);

                var process = GetProcessByRunId(parameters.run_id);
                if (process != null) process.Status = processStatus;

                return HttpStatusCode.OK;
            };
        }
        #region Helper Methods
        public List<string> GetAllProcessNames()
        {
            var processInstances = ProcessInstanceRepo.GetItems();
            var processNames = processInstances.Select(processInstance => processInstance.Name).ToList();
            return processNames;
        }

        public ProcessDefinition GetProcessByName(string name)
        {
            var processInstanceSchema = ProcessSchemaRepo.GetItem(); 
            var processInstanceSchemaJson = JsonConvert.SerializeObject(processInstanceSchema);
            var processInstance = ProcessInstanceRepo.GetItem(name); 
            var processInstanceJson = JsonConvert.SerializeObject(processInstance);

            var taskInstances = TaskInstanceRepo.GetItems(name);

            var taskDefs = new List<TaskDefinition>();
            foreach (var taskInstance in taskInstances)
            {
                var taskDef = new TaskDefinition();
                
                var instanceJson = JsonConvert.SerializeObject(taskInstance);
                taskDef.Instance = JObject.Parse(instanceJson);

                taskDef.InstanceName = taskInstance.Name;
                taskDef.SchemaName = taskInstance.TaskSchemaName;


                var schema = TaskSchemaRepo.GetItem(taskInstance.TaskSchemaName);
                
                var schemaJson = JsonConvert.SerializeObject(schema);
                
                taskDef.Schema = JObject.Parse(schemaJson);

                taskDefs.Add(taskDef);
            }
            
            var processDefinition = new ProcessDefinition
            {
                ProcessInstanceSchema = JObject.Parse(processInstanceSchemaJson),
                ProcessInstance = JObject.Parse(processInstanceJson),
                TaskDefinitions = taskDefs
            };
            return processDefinition;
        }
        public IWorkflowProcessInstance GetProcessByRunId(string runId)
        {
            var run = RunRepo.GetItem(runId,false);
            var processInstance = ProcessInstanceRepo.GetItem(run.ProcessInstanceName);
            return processInstance;
        }
        //TODO: this is temporary and once we get rid
        //TODO: of ProcessStatus then we wont need it
        public ProcessStatus GetProcessStatusByRunId(string runId)
        {
            var processStatus = new ProcessStatus();
            var runDetails = RunRepo.GetItem(runId,true);
            //Convert LogEntries to ProcessStatus
            var entries = runDetails.LogEntries;
            foreach (var logEntry in entries)
            {
                processStatus.Current = "[" + logEntry.Timestamp.ToString("o") + "] : " + logEntry.Message;
                processStatus.Full += processStatus.Current + "\n";
            }
            return processStatus;
        }
        public void PostProcess(string processPath, string oldProcessPath, ProcessDefinition processDefinition)
        {

            //check to see if we are in a renaming condition
            if (oldProcessPath != processPath)
            {
                //Rename folder
                if (Directory.Exists(oldProcessPath))
                    Directory.Move(oldProcessPath, processPath);
            }

            var processInstancePath = processPath + "\\processinstance.json";

            if (!Directory.Exists(processPath))
                Directory.CreateDirectory(processPath);

            File.WriteAllText(processInstancePath, processDefinition.ProcessInstance.ToString());
        }
        public static void DeleteAllTaskInstances(string processPath)
        {
            var Tasks = Directory.GetFiles(processPath, "*.json");
            var TaskList = Tasks.ToList();
            TaskList.Remove(processPath + "\\processinstance.json");

            foreach (var Task in TaskList)
            {
                if (File.Exists(Task))
                {
                    File.Delete(Task);
                }
            }
        }
        #endregion
    }

}
