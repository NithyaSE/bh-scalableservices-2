﻿using Newtonsoft.Json.Linq;

namespace BH.ScalableServices.Brokers.Workflow.Models
{
    public class TaskDefinition
    {
        public string SchemaName { get; set; }
        public string InstanceName { get; set; }
        public JObject Schema { get; set; }
        public JObject Instance { get; set; }
    }
}
