﻿using System;
using System.Configuration;
using System.Linq;

namespace BH.ScalableServices.Brokers.Workflow.Shared.Models
{

    public class WorkflowSettings
    {

        public Configuration Config { get; set; }

        public WorkflowSettings()
        {
        }

        public WorkflowSettings(Configuration config)
        {
            Config = config;
        }

        
        public void LoadSettings(string applicationName)
        {

            //"BH.Workflow.Process.exe"
            var exePath = System.IO.Path.Combine(
            AppDomain.CurrentDomain.BaseDirectory, applicationName);

            var config = Config ?? ConfigurationManager.OpenExeConfiguration(exePath);

            var props = GetType().GetProperties();

            //use reflection to loop through each property 
            //and populate its value for external app config
            foreach (var prop in props)
            {
                var propertyName = prop.Name;

                if (!config.AppSettings.Settings.AllKeys.Contains(propertyName)) continue;
                var value = config.AppSettings.Settings[propertyName].Value;
                prop.SetValue(this, value, null);
            }
        }


        public string ProcessBundlesLibraryLocation { get; set; }

        public string ProcessBundlesSubProcLocation { get; set; }

        public string ProcessRunsLocation { get; set; }

        public string TaskRunTempLocation { get; set; }
        
        public string UploadsLocation { get; set; }

        public string TasksLocation { get; set; }

        public string SchemasLocation { get; set; }

        public string BloodhoundDataStoreEndpoint { get; set; }

        public string BloodhoundDataStoreUsername { get; set; }

        public string BloodhoundDataStorePassword { get; set; }

        public string ExternalApiKey { get; set; }

        public string InternalApiKey { get; set; }
        
        public string WorkflowSchedulerBaseUri { get; set; }

        public string WorkflowSchedulerUri { get; set; }

        public string WorkflowSchedulerCreds { get; set; }

        public string WorkflowSchedulerTargetUrl { get; set; }
        public string WorkflowSchedulerTargetCreds { get; set; }

        public string DocumentProductionBaseUri { get; set; }

        public string DocumentProductionUri { get; set; }

        public string DocumentProductionCreds { get; set; }

        public string VisualizationApiBaseUrl { get; set; }

        public string VisualizationUri { get; set; }

        public string VisualizationApiCreds { get; set; }
        public string MessageRelayBaseUrl { get; set; }
        public string MessageRelayApiCreds { get; set; }
        public string MessageRelayUri { get; set; }
    }
}
