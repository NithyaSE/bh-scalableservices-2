﻿using Newtonsoft.Json.Linq;

namespace BH.ScalableServices.Workers.Plugins.Workflow.Shared.Interfaces
{
    /// <summary>
    /// Represents the type returned by a task when it is run
    /// </summary>
    public interface IRunTaskReturn
    {
        JObject WorkingSet { get; set; }
    }
}
