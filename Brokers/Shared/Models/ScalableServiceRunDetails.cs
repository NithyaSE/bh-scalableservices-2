﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace BH.ScalableServices.Brokers.Shared.Models
{
    public class ScalableServiceRunDetails

    {
    [JsonProperty("run_id")]
    public string RunId { get; set; }

    [JsonProperty("process_instance_name")]
    public string ProcessInstanceName { get; set; }

    [JsonProperty("enqueued_timestamp")]
    public DateTimeOffset EnqueuedTimestamp { get; set; }

    [JsonProperty("execution_started_timestamp")]
    public DateTimeOffset ExecutionStartedTimestamp { get; set; }

    [JsonProperty("execution_ended_timestamp")]
    public DateTimeOffset ExecutionEndedTimestamp { get; set; }

    [JsonProperty("total_queue_time_ms")]
    public double TotalQueueTimeMs { get; set; }

    [JsonProperty("total_execution_time_ms")]
    public double TotalExecutionTimeMs { get; set; }

    [JsonProperty("log_entries")]
    public List<ScalableServiceLogEntry> LogEntries { get; set; }

    [JsonProperty("status", NullValueHandling = NullValueHandling.Ignore)]
    public ScalableServiceStatus Status { get; set; }

    public void CalculateTotalTimes()
    {
        if (Status.IsDehydrated == true || Status.IsExecuting == true)
        {
            TotalQueueTimeMs =
                (ExecutionStartedTimestamp - EnqueuedTimestamp).TotalMilliseconds;
            TotalExecutionTimeMs = (DateTime.UtcNow - ExecutionStartedTimestamp).TotalMilliseconds;
        }
        else if (Status.IsQueued == true)
        {
            TotalQueueTimeMs = (DateTime.UtcNow - EnqueuedTimestamp).TotalMilliseconds;
            TotalExecutionTimeMs = 0;
        }
        else
        {
            TotalQueueTimeMs =
                (ExecutionStartedTimestamp - EnqueuedTimestamp).TotalMilliseconds;
            TotalExecutionTimeMs = (ExecutionEndedTimestamp - ExecutionStartedTimestamp).TotalMilliseconds;
        }
    }

    }
}
