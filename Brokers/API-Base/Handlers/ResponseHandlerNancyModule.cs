﻿using System;
using BH.ScalableServices.Brokers.APIBase.Models;
using Nancy;
using Newtonsoft.Json;

namespace BH.ScalableServices.Brokers.APIBase.Handlers
{
    public class ResponseHandlerNancyModule : NancyModule
    {
        public Response HandleResponse(Exception ex)
        {
            var response = new ExceptionResponse();

            response.Error = ex.ToString();
            response.StatusCode = HttpStatusCode.InternalServerError;

            return Response.AsJson(response).WithStatusCode(response.StatusCode);
        }

        public Response HandleErrorResponse(string message, HttpStatusCode statusCode)
        {
            var response = new ExceptionResponse()
            {
                StatusCode = statusCode,
                Error = message
            };

            return Response.AsJson(response).WithStatusCode(statusCode);
        }


        public Response HandleJSONResponse(object message, HttpStatusCode statusCode)
        {
            JsonConvert.SerializeObject(message);
            return Response.AsJson(message).WithStatusCode(statusCode).WithContentType("application/json");
        }

    }



}
