﻿namespace BH.ScalableServices.Brokers.Workflow.Shared.Interfaces
{
    public interface IRelocator<T>
    {
        void Relocate<T>(string oldPath, string newPath);
    }

}
