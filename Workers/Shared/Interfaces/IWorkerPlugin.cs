﻿using Amazon.SQS.Model;
using BH.ScalableServices.Brokers.Shared.Models;
using Newtonsoft.Json.Linq;

namespace BH.ScalableServices.Workers.Shared.Interfaces
{
    public interface IWorkerPlugin
    {
        void ProcessMessage(ScalableServiceRunMessage<JToken> message);
    }
}
