﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using SystemWrapper.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using BH.ScalableServices.Brokers.Workflow.Shared.Interfaces;
using BH.ScalableServices.Brokers.Workflow.Shared.Models;

namespace BH.ScalableServices.Brokers.Workflow.Shared.Respositories
{
    public class ProcessRunsRepository : IProcessRunsRepository
    {
        public IFileWrap FileIO { get; set; }
        public IDirectoryWrap DirIO { get; set; }
        public WorkflowSettings Settings { get; set; }
        static ReaderWriterLock rwl = new ReaderWriterLock();
        public ProcessRunsRepository(IFileWrap fileWrap, IDirectoryWrap dirWrap, WorkflowSettings settings)
        {
            FileIO = fileWrap;
            DirIO = dirWrap;
            Settings = settings;
          //  Settings.LoadSettings("BH.Workflow.Process.exe");
        }
        public IWorkflowProcessInstance GetProcessInstanceFromRunId(string runId)
        {
            var processInstance = LoadProcessInstanceFromRunId(runId);
            return processInstance;
        }
        public void SaveProcessInstance(IWorkflowProcessInstance processInstance)
        {
            SaveProcessInstanceFromRunId(processInstance);
        }
        public IWorkflowTaskInstance GetTaskInstanceFromRunId(string runId, string taskInstanceName)
        {
            var processContextPath = Path.Combine(Settings.ProcessRunsLocation, runId);
            IWorkflowTaskInstance instance = null;
            if (!DirIO.Exists(processContextPath)) return null;
            var instanceFilePattern = "*_" + taskInstanceName + ".json";
            var instanceLoc = DirIO.GetFiles(processContextPath, instanceFilePattern).FirstOrDefault();
            if (instanceLoc != null)
            {
                instance = RepositoryHelper.LoadTaskInstance(FileIO, instanceLoc);
            }
            return instance;
        }
        public List<IWorkflowTaskInstance> GetTaskIntancesFromRunId(string processInstanceName)
        {
            var processContextPath = Path.Combine(Settings.ProcessRunsLocation, processInstanceName);
            var instances = new List<IWorkflowTaskInstance>();
            if (!DirIO.Exists(processContextPath)) return null;
            var instanceFiles = DirIO.GetFiles(processContextPath, "*.json").ToList();
            foreach (var instanceFile in instanceFiles)
            {
                if (instanceFile != null && !instanceFile.ToLower().EndsWith("processinstance.json"))
                {
                    instances.Add(RepositoryHelper.LoadTaskInstance(FileIO, instanceFile));
                }
            }
            return instances;
        }
        public string CreateTempTaskInstance(IWorkflowTaskInstance taskInstance, string runId)
        {
            var processRunLocation = Path.Combine(Settings.ProcessRunsLocation, runId);
            if (!Directory.Exists(processRunLocation))
            {
                throw new Exception($"The runs folder at location {processRunLocation} could not be found");
            }
            var guid = Guid.NewGuid().ToString();
            var tempDirectoryLocation = Path.Combine(processRunLocation, Settings.TaskRunTempLocation);
            if (!Directory.Exists(tempDirectoryLocation))
                Directory.CreateDirectory(tempDirectoryLocation);
            var tempTaskLocation = Path.Combine(tempDirectoryLocation, $"{guid}.json");
            var taskInstanceJson = JsonConvert.SerializeObject(taskInstance);
            FileIO.WriteAllText(tempTaskLocation, taskInstanceJson);
            return guid;
        }

        public void DehydrateTempTask(string runId, string currentTaskFileName, IWorkflowTaskInstance workflowTaskInstance)
        {
            var processRunLocation = Path.Combine(Settings.ProcessRunsLocation, runId);
            if (!Directory.Exists(processRunLocation))
            {
                throw new Exception($"The runs folder at location {processRunLocation} could not be found");
            }
            var tempTaskFilePath = Path.Combine(processRunLocation, Settings.TaskRunTempLocation, $"{currentTaskFileName}.json");
            var taskInstanceJson = JsonConvert.SerializeObject(workflowTaskInstance);
            FileIO.WriteAllText(tempTaskFilePath, taskInstanceJson);
        }

        public IWorkflowTaskInstance RehydrateTempTask(string runId, string currentTaskFileName)
        {
            var processRunLocation = Path.Combine(Settings.ProcessRunsLocation, runId);
            var tempTaskFilePath = Path.Combine(processRunLocation, Settings.TaskRunTempLocation, $"{currentTaskFileName}.json");
            if (File.Exists(tempTaskFilePath))
                return RepositoryHelper.LoadTaskInstance(FileIO, tempTaskFilePath);
            throw new Exception($"The current task file name {currentTaskFileName} provided in process is invalid.  The corresponding temp file does not exist.");
        }
        public void RemoveTempTaskFile(string runId, string currentTaskFileName)
        {
            var processRunLocation = Path.Combine(Settings.ProcessRunsLocation, runId);
            var tempTaskFilePath = Path.Combine(processRunLocation, Settings.TaskRunTempLocation, $"{currentTaskFileName}.json");
            if(File.Exists(tempTaskFilePath))
                File.Delete(tempTaskFilePath);
        }
        public bool IsResumeTokenValid(string resumeToken,string runId)
        {
            //   Log.Logger.AddMethodName.Debug($"Acquiring writer lock over repository  for runId { runId}");
            try
            {
                rwl.AcquireWriterLock(0); // The thread will error out immediately if the lock cannot be acquired. In case of duplicate calls..
                try
                {
                    var processInstance = LoadProcessInstanceFromRunId(runId);
                    processInstance.RunId = runId;
                    if (processInstance.ProcessVariables.ContainsKey("$resume_token"))
                    {
                        var resumeTokenOnProcessInstance = processInstance.ProcessVariables["$resume_token"].ToString();
                        if (!string.IsNullOrWhiteSpace(resumeTokenOnProcessInstance) && resumeTokenOnProcessInstance.Equals(resumeToken))
                        {
                            //// remove the resume token from the process instance.
                            //processInstance.ProcessVariables.Remove("$resume_token");
                            //SaveProcessInstance(processInstance);
                            return true;
                        }
                        else
                        {
                            throw new Exception($"The resume token {resumeToken} is invalid for runId {runId}. Resume token not found on process {processInstance.Name} associated with the runId");
                        }
                    }
                    else
                    {
                        throw new Exception($"The resume token {resumeToken} is invalid for runId {runId}. Resume token not found on process {processInstance.Name} associated with the runId");
                    }
                }
                finally
                {
                    // Ensure that the lock is released.
                  //  LogUtility.WriteToLog($"Releasing writer lock over repository  for runId { runId}");
                    rwl.ReleaseWriterLock();
                }
            }
            catch (ApplicationException ex)
            {
                throw new Exception($"Another request for resuming the runId {runId} with resume token {resumeToken} already in progress.");
            }
        }
        public void UpdateWorkingSet(string taskName, string runId,string currentTaskFileName, string body)
        {
            rwl.AcquireWriterLock(0); // The thread will error out immediately if the lock cannot be acquired. In case of duplicate calls..
            try
            {
                var taskInstance = RehydrateTempTask(runId,currentTaskFileName);
                var updatedWorkingSet = JObject.Parse(body);
                taskInstance.WorkingSet = updatedWorkingSet;
                DehydrateTempTask(runId,currentTaskFileName,taskInstance);
            }
            finally
            {
                // Ensure that the lock is released.
                //  LogUtility.WriteToLog($"Releasing writer lock over repository  for runId { runId}");
                rwl.ReleaseWriterLock();
            }
        }
        private IWorkflowProcessInstance LoadProcessInstanceFromRunId(string runId)
        {
            var files = RepositoryHelper.GetAllFilesUnderLocation(DirIO,Path.Combine(Settings.ProcessRunsLocation, runId));
            foreach (var file in files)
            {
               var filename = Path.GetFileName(file ?? "").ToLower();
                if (!filename.StartsWith("processinstance") || !filename.EndsWith(".json")) continue;
                if (file == null) continue;
                var processInstance = RepositoryHelper.LoadProcessInstance(FileIO,file);
                return processInstance;
            }
            return null;
        }
        private void SaveProcessInstanceFromRunId(IWorkflowProcessInstance processInstance)
        {
            RepositoryHelper.SaveProcessInstance(FileIO, Path.Combine(Settings.ProcessRunsLocation, processInstance.RunId, "processinstance.json"), processInstance);
        }

    }
}
