﻿using Newtonsoft.Json;

namespace BH.ScalableServices.Brokers.Workflow.Shared.Models
{
    public class ProcessStatus
    {
        [JsonProperty("full")]
        public string Full { get; set; }

        [JsonProperty("current")]
        public string Current { get; set; }

    }
}
