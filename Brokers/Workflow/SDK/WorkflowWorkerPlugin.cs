﻿using System;
using System.IO;
using SystemWrapper.IO;
using Amazon.SQS.Model;
using BH.ScalableServices.Workers.Plugins.Workflow.Interfaces;
using BH.ScalableServices.Workers.Plugins.Workflow.Models;
using Newtonsoft.Json;
using Serilog;
using Shared.SerilogsLogging;
using TinyIoC;
using BH.ScalableServices.Workers.Models;
using BH.ScalableServices.Models;
using BH.ScalableServices.Workers.Helpers;
using BH.ScalableServices.Brokers.SDKBase;
using BH.ScalableServices.Brokers.Workflow.Shared.Models;
using BH.ScalableServices.Brokers.Workflow.Shared.Interfaces;
using BH.ScalableServices.Brokers.Workflow.Shared.Respositories;
using BH.Shared.PluginRouter;

namespace BH.ScalableServices.Workers.Plugins.Workflow
{
    /// <summary>
    /// Plugin for Worflow worker
    /// </summary>
    public class WorkflowWorkerPlugin : IWorkerPlugin
    {
        private static string AssemblyFullPath;
        private static string AssemblyBasePath;
        public WorkflowWorkerPlugin()
        {
            var codeBase = typeof(WorkflowWorkerPlugin).Assembly.CodeBase;
            var uri = new UriBuilder(codeBase);
            AssemblyFullPath = Uri.UnescapeDataString(uri.Path);
            AssemblyBasePath = Path.GetDirectoryName(AssemblyFullPath);
        }
        public void ProcessMessage(Message message) //  change from message to json string?
        {
            //desrialize to WFS request
            var wfsReq = JsonConvert.DeserializeObject<RunMessage<string>>(message.Body);
            var runId = wfsReq?.RunId;
            var processName = wfsReq?.Message;
            var workerAppDomain = AppDomain.CreateDomain("workerAppDomain", null, AssemblyBasePath, null, false);
            workerAppDomain.SetData("RunId", runId);
            workerAppDomain.SetData("AssemblyFullPath", AssemblyFullPath);
            try
            {
                workerAppDomain.DoCallBack(() =>
                {
                    try
                    {
                        StatusUpdater.ServiceWebClient = new ScalableServiceWebClient();
                        LogEntryWriter.ServiceWebClient = StatusUpdater.ServiceWebClient;
                        var appDomainRunId = (string)AppDomain.CurrentDomain.GetData("RunId");
                        AssemblyFullPath = (string)AppDomain.CurrentDomain.GetData("AssemblyFullPath");
                        RunProcess(appDomainRunId);
                    }
                    finally
                    {
                        try
                        {
                            Log.CloseAndFlush();
                        }
                        catch(Exception ex)
                        {
                            Log.Logger.AddMethodName().Fatal("Summary='Error Occured while while flushing the logger for the app domain.' Details='{@Ex}'", ex);
                        }
                    }
                });
            }
            catch (Exception ex)
            {
                Log.Logger.AddMethodName().Fatal("Summary='Error occured while running the process {@ProcessName} in a new App Domain' Details='{@Ex}'", processName, ex);
            }
            finally
            {
                Log.Logger.AddMethodName().Information("Summary='Unloading the app domain.'");
                AppDomain.Unload(workerAppDomain);
            }            
        }
        private static void RunProcess(string runId)
        {
            var container = Bootstrap();
            var processRunsRepo = container.Resolve<IProcessRunsRepository>();
            // Use the repository to get the process instance from process runner 
            var workflowProcessInstance = processRunsRepo.GetProcessInstanceFromRunId(runId);
            workflowProcessInstance.RunId = runId;
            var workflowProcessRunner = container.Resolve<WorkflowRunner>();
            workflowProcessRunner.WorkflowProcessInstance = workflowProcessInstance;
            workflowProcessRunner.RunProcess();
        }
        protected static TinyIoCContainer Bootstrap()
        {
            var container = TinyIoCContainer.Current;
            container.Register<IApiRequest, ApiRequest>();
            container.Register<IWorkflowServiceClient, WorkflowServiceClient>();
            container.Register<IPluginRouter, PluginRouter>();
            container.Register<IFileWrap, FileWrap>();
            container.Register<IDirectoryWrap, DirectoryWrap>();
            container.Register<IWorkflowProcessInstance, WorkflowProcessInstance>();
            container.Register<IWorkflowTaskInstance, WorkflowTaskInstance>();
            var settings = new WorkflowSettings();
            settings.LoadSettings(AssemblyFullPath);
            Setup(settings);
            Log.Logger = new LoggerConfiguration().ReadFrom.AppSettings().CreateLogger();
            container.Register(settings);
            container.Register<IProcessRunsRepository, ProcessRunsRepository>();
            container.Register<WorkflowRunner>();
            return container;
        }
        protected static void Setup(WorkflowSettings settings)
        {
            //this was added to get away from 2 different config files
            WorkflowRunner.TasksLocation = settings.TasksLocation;
            JsonConvert.DefaultSettings = () => new JsonSerializerSettings
            {
                DateParseHandling = DateParseHandling.None
            };
        }
    }
}
