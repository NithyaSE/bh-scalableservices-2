﻿using System.Collections.Generic;
using Shared.AmazonSQS;
using BH.ScalableServices.Brokers.APIBase.Handlers;
using BH.ScalableServices.Brokers.APIBase.Interfaces;
using BH.ScalableServices.Brokers.Shared.Models;
using Newtonsoft.Json;
using BH.ScalableServices.Brokers.Shared.Enums;

namespace BH.ScalableServices.Brokers.APIBase.Endpoints
{
    /// <summary>
    /// 
    /// </summary>
    public abstract class StartEndpoints : ResponseHandlerNancyModule
    {
        private IRunDetailsRepository RunRepo { get; set; }

        protected abstract string RouteVersion { get; }

        /// <summary>
        /// 
        /// </summary>
        public StartEndpoints(IRunDetailsRepository runRepo)
        {
            RunRepo = runRepo;
        }

        public ScalableServiceRunDetails StartProcess<T>(string runId,T message, string name, string queue,WorkerType workerType) // othen info to send to queue
        {
            var run = GenerateNewRunMessage(runId,message, name,workerType);
            var runDetails = GenerateNewRunDetails(name, run.RunId);

            var SQS = new AmazonSQSHelper( );
            SQS.WriteMessageToQueue(JsonConvert.SerializeObject(run));

            return runDetails;
        }

        private ScalableServiceRunMessage<T> GenerateNewRunMessage<T>(string runId,T message, string name,WorkerType workerType)
        {
            var run = new ScalableServiceRunMessage<T>
                {
                    RunId = runId,
                    Name = name,
                    Message = message,
                    WorkerType = workerType

                };

            return run;
        }

        private ScalableServiceRunDetails GenerateNewRunDetails(string name, string runId)
        {
            var rundetails = new ScalableServiceRunDetails
            {
                RunId = runId,
                ProcessInstanceName = name,
                LogEntries = new List<ScalableServiceLogEntry>()
                //EnqueuedTimestamp = DateTime.UtcNow
            };

            var status = new ScalableServiceStatus
            {
                IsQueued = true,
                IsExecuting = false
            };
            RunRepo.AddUpdateStatus(rundetails.RunId, status, name);
            // to keep run status in a separate file from run details, the status is set on rundetails after serialization
            var runDetailsResp = new ScalableServiceRunDetails();
            runDetailsResp.RunId = rundetails.RunId;
            runDetailsResp.ProcessInstanceName = rundetails.ProcessInstanceName;
            runDetailsResp.LogEntries = rundetails.LogEntries;
            runDetailsResp.Status = status;
            return runDetailsResp;
        }

    }


}
