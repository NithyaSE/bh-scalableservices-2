﻿using System.Collections.Generic;
using BH.ScalableServices.Workers.Plugins.Workflow.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace BH.ScalableServices.Workers.Plugins.Workflow.Models
{
    public class WorkflowTaskInstance : IWorkflowTaskInstance
    {
        public WorkflowTaskInstance()
        {
        }
        public WorkflowTaskInstance(string taskDllName)
        {
            TaskDllName = taskDllName;
        }
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("working_set")]
        public JObject WorkingSet { get; set; }
        [JsonProperty("schema_name")]
        public string TaskSchemaName { get; set; }
        [JsonProperty("is_active")]
        public BHWorkflowTaskMappableVariable<bool> IsActive { get; set; }
        [JsonProperty("working_set_input_mappings_to_process_variables")]
        public JObject WorkingSetInputMappingsToPvs { get; set; }
        [JsonProperty("working_set_output_mappings_to_process_variables")]
        public IDictionary<string, string> WorkingSetOutputMappingsToPvs { get; set; }  // output property name and pv name
        [JsonProperty("system_task_variables")]
        public JObject SystemTaskVariables { get; set; }
        [JsonProperty("dll_name")]
        public string TaskDllName { get; private set; }

    }
}