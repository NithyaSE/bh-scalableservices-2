﻿using System.Collections.Generic;
using BH.ScalableServices.Workers.Plugins.Workflow.Shared.Models;
using Newtonsoft.Json;

namespace BH.ScalableServices.Workers.Plugins.Workflow.Tasks.RunSubProcess.v2_0.Models
{
    public class SubProcessWorkingSet 
    {
        [JsonProperty("sub_process_process_name")]
        public string SubProcessProcessName { get; set; }
        [JsonProperty("iterator")]
        public List<string> Iterator { get; set; }
        [JsonProperty("sub_process_process_variable_mappings")]
        public List<KeyValueObject<List<KeyValueObject<object>>>> SubProcessProcessVariableMappings {get;set;}
        [JsonProperty("check_interval")]
        public string CheckInterval { get; set; }
        [JsonProperty("sub_process_registry")]
        public List<SubProcessRegistryItem> SubProcessRegistry { get; set; }
    }
}
