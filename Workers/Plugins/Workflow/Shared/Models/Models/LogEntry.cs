﻿using System;
using Newtonsoft.Json;

namespace BHX.Workflow.Shared.Models.Models
{

    public class LogEntryV1
    {
        [JsonProperty("timestamp")]
        public DateTimeOffset Timestamp { get; set; }

        [JsonProperty("entry")]
        public string Entry { get; set; }
    }
}
