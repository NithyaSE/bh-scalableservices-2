﻿namespace BH.ScalableServices.Workers.Plugins.Workflow.Models
{
    public enum ProcessLogType
    {
        Info,
        Error,
        Warning
    }
}
