@ECHO OFF
set confarg=%1
set configuration=Debug
set localbuildarg=%2
set localbuild=true

if "%localbuildarg%"=="false" (set localbuild=false)
if "%localbuildarg%"=="FALSE" (set localbuild=false)
if "%localbuildarg%"=="False" (set localbuild=false)

if "%confarg%"=="release" (set configuration=Release)
if "%confarg%"=="Release" (set configuration=Release)

del .\localBuildBloodhound\bin\*.* /Q > NUL 2>&1


REM remove the following line for debugging

@ECHO ON
rem msbuild BHScalableServicesWFSTaskDocProd2.2.proj /P:configuration="%configuration%";Platform="Any CPU";FileVersionMajorNumber="7";FileVersionMinorNumber="7";TFSBuildNumber="7777";TFSBuildRevisionNumber="7";LocalBuild=%localbuild%;Configuration=%configuration%
msbuild BHScalableServicesWFSTaskDocProd2.2.proj /P:configuration="%configuration%";Platform="Any CPU";LocalBuild=%localbuild%;Configuration=%configuration%
@ECHO OFF
@ECHO ON
SET completed_at=BHScalableServicesWFSTaskDocProd2.2.bat %configuration% LocalBuild=%localbuild% completed at: %date% %time%
