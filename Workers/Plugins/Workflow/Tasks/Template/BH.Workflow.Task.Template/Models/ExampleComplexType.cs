﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace BH.Workflow.Task.Template.Models
{
    /// <summary>
    /// Example Object to show how objects can be modeled in a Task
    /// </summary>
    public class ExampleComplexType
    {
            [JsonProperty("example_prop_str")]
            public string ExamplePropStr { get; set; }

            [JsonProperty("example_prop_list")]
            public List<string> ExamplePropList { get; set; }
        
    }
}
