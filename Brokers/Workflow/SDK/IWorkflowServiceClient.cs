﻿using BH.ScalableServices.Brokers.Workflow.Shared.Interfaces;
using System.Collections.Generic;
using BH.ScalableServices.Brokers.SDKBase;
using BH.ScalableServices.Brokers.Shared.Models;

namespace BH.ScalableServices.Brokers.Workflow.SDK
{
    public interface IWorkflowServiceClient :IScalableServiceWebClient
    {
        IWorkflowProcessInstance GetProcessInstance(string processInstanceName); 
        IWorkflowTaskInstance GetTaskInstance(string processInstanceName, string taskInstanceName);
        List<IWorkflowTaskInstance> GetTaskInstances(IWorkflowProcessInstance processInstance);
        void PostProcessOutputs(IWorkflowProcessInstance processInstance);
        Dictionary<string, object> GetProcessOutputs(string runId);
        ScalableServiceRunDetails StartSubProcess(string processName, string partialPath);

    }
}
