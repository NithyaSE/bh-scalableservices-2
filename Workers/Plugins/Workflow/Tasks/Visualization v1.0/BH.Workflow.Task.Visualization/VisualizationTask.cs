﻿using BH.ScalableServices.Brokers.SDKBase;
using BH.ScalableServices.Brokers.Shared.Models;
using BH.ScalableServices.Workers.Plugins.Workflow.Shared.Interfaces;
using BH.ScalableServices.Workers.Plugins.Workflow.Shared.Models;
using BH.ScalableServices.Workers.Plugins.Workflow.Tasks.Visualization.v1_0.Models;
using BH.ScalableServices.Workers.Shared.Helpers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Shared.WebClient;
using Shared.WebClient.Models;
using System;
using System.Net.Http;
using System.Threading;

namespace BH.ScalableServices.Workers.Plugins.Workflow.Tasks.Visualization.v1_0
{
    public class VisualizationTask : WorkflowTask
    {
       public override IRunTaskReturn RunTask(JObject workingSet)
        {
            Console.WriteLine($"Running Visualization Task: {SystemTaskVariables.WorkflowTaskInstanceName}");
            var vizWorkingSet = workingSet.ToObject<VisualizationTaskWorkingSet>();
            var baseUri = Settings.VisualizationApiBaseUrl;
            var creds = Settings.VisualizationApiCreds;
            var vizApi = new ScalableServiceWebClient();
            vizApi.Setup(baseUri, creds);
            var body = new
            {
                datasource_instance = JsonConvert.DeserializeObject<JObject>(vizWorkingSet.DatasourceInstance) ,
                widget_instance = JsonConvert.DeserializeObject<JObject>(vizWorkingSet.WidgetInstance),
                file_properties = vizWorkingSet.FileProperties ,
                repository = vizWorkingSet.Repository

            };
            var request = SetupVisualizationApiRequest(body);
            var runDetails = JsonConvert.DeserializeObject<ScalableServiceRunDetails>(HttpHelper.HandleHttpResponse(HttpHelper.HandleRequest(request)));
            try
            { 
            var runId = runDetails.RunId;
            LogEntryWriter.LogMessage($"RunId of the Visualization Api Request: {runId}. Use this RunId to get the run details of the Visualization request.",SystemTaskVariables.ProcessRunId);
            while (true)
                {
                    Thread.Sleep(1000);
                    runDetails = vizApi.GetDetails(runId);
                    if (runDetails == null || runDetails.Status == null)
                    {
                        continue;
                    }
                    if (runDetails.Status.IsException == true)
                    {
                        LogEntryWriter.LogMessage($"Status of the Visualization Request : {JsonConvert.SerializeObject(runDetails.Status)}",SystemTaskVariables.ProcessRunId);

                        throw new Exception($"The Visualization request returned with 'is_exception=TRUE'," +
                             $" which caused the task to throw this exception. Use the RunId provided above to get the error details.");
                    }
                    if (runDetails.Status.IsQueued == false && runDetails.Status.IsExecuting == false)
                    {
                        break;
                    }
                    Thread.Sleep(4000);
            }
            }
            catch (Exception exp)
            {
                LogEntryWriter.LogMessage(exp.Message, SystemTaskVariables.ProcessRunId);
                throw;
            }
            return new FinishRunTaskReturn()
             {
                WorkingSet = JObject.FromObject(vizWorkingSet)
            };
            }
        private HttpRequest SetupVisualizationApiRequest(dynamic body)
        {
            var baseUri = Settings.VisualizationApiBaseUrl;
            var uri = Settings.VisualizationUri;
            var creds = Settings.VisualizationApiCreds;
            var request = new HttpRequest(HttpVerb.Post) { BaseUri = baseUri };
            request.PathFragments.Add(new HttpPathFragment("uri", uri)); // URI to start
            if (creds != null)
                request.Headers.Add(new HttpHeader("Authorization", creds));
            request.Body = new StringContent(JsonConvert.SerializeObject(body));
            return request;
        }
     }
}
