﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using BH.ScalableServices.Brokers.Workflow.SDK;
using BH.ScalableServices.Brokers.Workflow.Shared.Models;
using BH.ScalableServices.Workers.Plugins.Workflow.Shared.Interfaces;
using BH.ScalableServices.Workers.Plugins.Workflow.Shared.Models;
using BH.ScalableServices.Workers.Plugins.Workflow.Tasks.RunSubProcess.v2_0.Models;
using BH.ScalableServices.Workers.Shared.Helpers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace BH.ScalableServices.Workers.Plugins.Workflow.Tasks.RunSubProcess.v2_0
{
    public class RunSubProcesses : WorkflowTask
    {
   public override IRunTaskReturn RunTask(JObject workingSet)
        {
            var subProcessWorkingSet = workingSet.ToObject<SubProcessWorkingSet>();
            //first run
            var checkInterval = subProcessWorkingSet.CheckInterval;
            var regex = new Regex("^P(?!$)(\\d+Y)?(\\d+M)?(\\d+W)?(\\d+D)?(T(?=\\d+[HMS])(\\d+H)?(\\d+M)?(\\d+S)?)?$");
            var match = regex.Match(checkInterval);
            if (!match.Success)
            {
                throw new Exception($"Invalid format of check interval {checkInterval}. Only ISO interval format accepted");
            }
            if (!SystemTaskVariables.IsRehydrated)
            {
                var iterator = subProcessWorkingSet.Iterator; // iterator list of strings
                var duplicates = iterator?.GroupBy(i => i)
                    .Where(g => g.Count() > 1)
                    .Select(y => new { iterator = y.Key, Count = y.Count() })
                    .ToList();
                if (duplicates != null && duplicates.Count > 0)
                {
                    throw new Exception($"Failed to run SubProcess task. Following duplicates found in the list of iterators. {JsonConvert.SerializeObject(duplicates)}");
                }
                var subProcessProcessVariableMappings = subProcessWorkingSet.SubProcessProcessVariableMappings; // <name, <key, value>>
                var subProcessName = subProcessWorkingSet.SubProcessProcessName;
                var subProcRegistry = new List<SubProcessRegistryItem>();
                foreach (var iteration in iterator)
                {
                    InitSubProcess(subProcRegistry, iteration, subProcessName, subProcessProcessVariableMappings);
                }
                subProcessWorkingSet.SubProcessRegistry = subProcRegistry;
                return new DehydrateRunTaskReturn()
                {
                    CheckInterval = checkInterval,
                    WorkingSet =  JObject.FromObject(subProcessWorkingSet)
                };
            }
            else
            {
                var subProcRegistry = subProcessWorkingSet.SubProcessRegistry;
               // get all runIds for unfinished processes
                var runningSubProcRegistry = subProcRegistry?.Where(item => !item.IsFinished);
                foreach (var subProcRegistryItem in runningSubProcRegistry)
                {
                    CheckSubProcRunStatus(subProcRegistryItem);
                }
                var numFinish = subProcRegistry?.Where(item => item.IsFinished).Count();
                if (numFinish != subProcRegistry.Count)
                    return new DehydrateRunTaskReturn()
                    {
                        CheckInterval = checkInterval,
                        WorkingSet = JObject.FromObject(subProcessWorkingSet)
                    };
                var outputs = CollectSubProcessOutputs(subProcRegistry);
                foreach (var subProcess in subProcRegistry)
                {
                    Directory.Delete(subProcess.Location, true);
                }
                LogEntryWriter.LogMessage($"All subprocesses complete.", SystemTaskVariables.ProcessRunId);
                foreach (var output in outputs)
                {
                    workingSet[output.Key] = JToken.FromObject(output.Value);
                }
                return new FinishRunTaskReturn()
                {
                    WorkingSet = workingSet
                };
            }
        }
        private void CheckSubProcRunStatus(SubProcessRegistryItem subProcRegistryItem)
        {
            //get details, if not done break
            var runDetails = StatusUpdater.ServiceWebClient.GetDetails(subProcRegistryItem.RunId,false);
            if (runDetails.Status.IsException == true  || runDetails.Status.IsWarning == true)
            {
                LogEntryWriter.LogMessage($"Iteration :{subProcRegistryItem.Iteration} - Error in subprocess : {runDetails.ProcessInstanceName}. Check RunId : {runDetails.RunId} for details.", SystemTaskVariables.ProcessRunId);
                var reason = runDetails.Status.IsException == true ? ("is_exception = TRUE") : (runDetails.Status.IsWarning == true ? "is_warning = TRUE" :"");
                throw new Exception($"An exception was raised in the subprocess task '{SystemTaskVariables.WorkflowTaskInstanceName}' as iteration '{subProcRegistryItem.Iteration}' completed with '{reason}'. Check RunId : '{runDetails.RunId}' for details.");
            }
            if (runDetails.Status.IsQueued != false || runDetails.Status.IsExecuting != false ||
                runDetails.Status.IsDehydrated != false) return;
            LogEntryWriter.LogMessage($"Iteration : '{subProcRegistryItem.Iteration}' - Subprocess : '{runDetails.ProcessInstanceName}' complete. Check RunId : '{runDetails.RunId}' for details.", SystemTaskVariables.ProcessRunId);
            subProcRegistryItem.IsFinished = true;
        }
        private Dictionary<string, Dictionary<string, object>> CollectSubProcessOutputs(List<SubProcessRegistryItem> subProcRegistry)
        {
            var outputRunIdDict = new Dictionary<string, Dictionary<string, object>>(); // <outputpv,<iteration,value>>
            foreach (var subProcessRegistryItem in subProcRegistry)
            {
                //get outputs and put into dictionary
                var outputs = ((WorkflowServiceClient)StatusUpdater.ServiceWebClient).GetProcessOutputs(subProcessRegistryItem.RunId); //<outputpv,value>
                foreach (var outputvar in outputs)
                {
                    var outputPvName = outputvar.Key;
                    if (!outputRunIdDict.ContainsKey(outputPvName))
                        outputRunIdDict.Add(outputPvName, new Dictionary<string, object>());
                    outputRunIdDict[outputPvName].Add(subProcessRegistryItem.Iteration, outputvar.Value);
                }
            }
            return outputRunIdDict;
        }
        private void InitSubProcess(List<SubProcessRegistryItem> subProcRegistry, string iteration,string baseProcessName,List<KeyValueObject<List<KeyValueObject<object>>>> inputs)
        {
            var name = iteration;
            var subProcRegistryItem = new SubProcessRegistryItem();
            //ceate new folder
            var newLocation = Path.Combine(Settings.ProcessBundlesSubProcLocation, SystemTaskVariables.ProcessRunId, baseProcessName, name);
            var partialPathToSubProcess = $"{SystemTaskVariables.ProcessRunId}\\{baseProcessName}";
            if (!Directory.Exists(newLocation))
                Directory.CreateDirectory(newLocation);
            //copy over base to new location
            var oldLocation = Path.Combine(Settings.ProcessBundlesLibraryLocation, baseProcessName);
            var files = Directory.GetFiles(oldLocation, "*", SearchOption.AllDirectories);
            foreach (var file in files)
            {
                File.Copy(new FileInfo(file).FullName, Path.Combine(newLocation, new FileInfo(file).Name), true);
            }
            subProcRegistryItem.Location = newLocation;
            //open it up and update the process variables defined in the dictionary and save... tricky
            var instanceFile = Path.Combine(newLocation, "processInstance.json");
            var json = File.ReadAllText(instanceFile);
            var instance = JsonConvert.DeserializeObject<WorkflowProcessInstance>(json);
            //Set Required process variable values to corresponding RequiredPV in parent process
            SetRequiredPVsOnSubProcessInstance(instance);
            if (inputs != null)
            {
                foreach (var mapping in inputs)
                {
                    //mapping.key is the PV name and mapping.value is the dictionary for iterations
                    var subprocessPvIterationValues = mapping.Value;
                    if(subprocessPvIterationValues == null) continue;
                    var numberOfMappings = subprocessPvIterationValues.Count(i => i.Key.Equals(iteration));
                    if (numberOfMappings == 0) continue;
                    if(numberOfMappings>1)
                    {
                        throw new Exception($"Multiple mappings found for iterator '{iteration}'. Only one mapping per iterator is allowed.");
                    }
                    var subProcessPvValueForCurrentIteration =
                        subprocessPvIterationValues
                            .FirstOrDefault(x => x.Key == iteration); // mapped value of PV for this iteration
                    instance.ProcessVariables[mapping.Key] = subProcessPvValueForCurrentIteration?.Value;
                }
            }
            // instance.PathName = newLocation;
            if (!instance.ProcessVariables.Any(x => x.Key == "$iteration"))
                instance.ProcessVariables.Add("$iteration", iteration);
            else
                instance.ProcessVariables["$iteration"]= iteration;
            json = JsonConvert.SerializeObject(instance);
            File.WriteAllText(instanceFile, json);
            //start the new process
            var newProcessRunDetails = ((WorkflowServiceClient)StatusUpdater.ServiceWebClient).StartSubProcess(name, partialPathToSubProcess);
            //keep track of run_ids
            subProcRegistryItem.RunId = newProcessRunDetails.RunId;
            subProcRegistryItem.Iteration = iteration;
            subProcRegistryItem.IsFinished = false;
            subProcRegistry.Add(subProcRegistryItem);
           LogEntryWriter.LogMessage($"Spawning Sub Process for iteration : {iteration} with Run Id : {newProcessRunDetails.RunId}", SystemTaskVariables.ProcessRunId);
        }
       
    }
}
