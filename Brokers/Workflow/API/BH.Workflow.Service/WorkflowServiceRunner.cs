﻿using System;
using System.Configuration;
using System.Threading;
using Shared.SerilogsLogging;
using Nancy.Hosting.Self;
using Serilog;

namespace BH.ScalableServices.Brokers.Workflow
{
    public class WorkflowServiceRunner
    {
        private Thread _runThread;
        protected NancyHost _host;

        public void Start()
        {
            var useSSL = Convert.ToBoolean(ConfigurationManager.AppSettings["SSL"]);
            
            var uri = useSSL ? new Uri("https://localhost:" + ConfigurationManager.AppSettings["Port"]) :
                               new Uri("http://localhost:" + ConfigurationManager.AppSettings["Port"]);

            _host = new NancyHost(uri);

            Console.WriteLine(@"Bloodhound Workflow Service Started");
            Console.WriteLine(@"Listening @ " + uri.AbsoluteUri);
            Log.Logger.AddMethodName().Information(@"Bloodhound Workflow Service Started");
            _host.Start();
        }
        public void Stop()
        {
            Console.WriteLine(@"Bloodhound Workflow Service Stopped");
            Log.Logger.AddMethodName().Information(@"Bloodhound Workflow Service Stopped");
            _host.Stop();
        }
    }
}
