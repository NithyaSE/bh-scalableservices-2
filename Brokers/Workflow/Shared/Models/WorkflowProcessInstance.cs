﻿using System;
using System.Collections.Generic;
using BH.ScalableServices.Brokers.Workflow.Shared.Interfaces;
using Shared.SerilogsLogging;
using Shared.Time;
using Newtonsoft.Json;
using Serilog;

namespace BH.ScalableServices.Brokers.Workflow.Shared.Models
{
    public class WorkflowProcessInstance : IWorkflowProcessInstance
    {
        [JsonProperty("name")]
        public string Name { get; private set; }
        [JsonProperty("is_active")]
        public bool IsActive { get; set; }
        [JsonProperty("task_execution_order")]
        public List<string> TaskExecutionOrder { get; set; }
        [JsonProperty("on_error_execution_order")]
        public List<string> OnErrorExecutionOrder { get; set; }
        // Update ProcessVariable to InputPV
        [JsonProperty("process_variables")]
        public Dictionary<string, object> ProcessVariables { get; set; }
        [JsonProperty("output_process_variables")]
        public List<PVWorkingSetInputOutputMapping> PVWorkingSetInputOutputMappings { get; set; }
        /// <summary>
        /// Note this is for backwards compatability and possibly for a foreign key
        /// </summary>
        [JsonIgnore]
        public string RunId { get; set; }
        public WorkflowProcessInstance()
        {
            TaskExecutionOrder = new List<string>();
            ProcessVariables = new Dictionary<string, object>();
            PVWorkingSetInputOutputMappings = new List<PVWorkingSetInputOutputMapping>();
        }
        public WorkflowProcessInstance(string name)
        {
            TaskExecutionOrder = new List<string>();
            ProcessVariables = new Dictionary<string, object>();
            PVWorkingSetInputOutputMappings = new List<PVWorkingSetInputOutputMapping>();
            Name = name;
        }
        public object GetProcessVariable(string key)
        {
            if (ProcessVariables == null || ProcessVariables.Count <= 0 || !ProcessVariables.ContainsKey(key))
            {
                Log.Logger.AddMethodName().Error("Summary='Error getting the value of PV {key}. The PV could not be found in the list of PVs'", key);
                throw new Exception($"Error getting value of PV {key} . The PV could not be found in the list of PVs.");
            }
            var value = ProcessVariables[key];
            return value;
        }
        /// <summary>
        /// Sets a process variable that exists in the list of process variables
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void SetProcessVariable(string key, object value)
        {
            if (ProcessVariables == null || !ProcessVariables.ContainsKey(key))
            {
                throw new Exception($"Error setting the value of PV {key} . The PV could not be found in the list of PVs.");
            }
            ProcessVariables[key] = value;
       }
        public void SetProcessDisplayName(string name)
        {
            Name = name;
        }
        public Dictionary<string, object> SetOutputs()
        {
            var dict = new Dictionary<string, object>();

            if (PVWorkingSetInputOutputMappings == null)
                PVWorkingSetInputOutputMappings = new List<PVWorkingSetInputOutputMapping>(); // TODO: Fix a better way
            foreach (var pvInputOutputMapping in PVWorkingSetInputOutputMappings)
            {
                if (ProcessVariables.ContainsKey(pvInputOutputMapping.ProcessVariableName))
                {
                    dict.Add(pvInputOutputMapping.OutputProcessVariableName, ProcessVariables[pvInputOutputMapping.ProcessVariableName]);
                    ProcessVariables[pvInputOutputMapping.ProcessVariableName] =
                        ProcessVariables[pvInputOutputMapping.ProcessVariableName];
                }
                else
                    throw new Exception($"Mapping Output Process Variable '{pvInputOutputMapping.OutputProcessVariableName}' to Input Process Variable '{pvInputOutputMapping.ProcessVariableName}' failed. Input Process Variable '{pvInputOutputMapping.ProcessVariableName}' not found in the list of Input Process Variables.");
            }

            return dict;
        }
        /// <summary>
        /// Checks for the timezone and looks up the keywords 
        /// which are same as the method name of the TimestampKeyword class
        /// </summary>
        public void ComputeProcessVariableKeywords(string timezone)
        {
            TimestampKeywords.TimeZoneInfo = Time.TimezoneLookup(timezone);

            var pvKeys = new List<string>(ProcessVariables.Keys);
            foreach (var keyProcessVariable in pvKeys)
            {
                var pvValueObj = ProcessVariables[keyProcessVariable];
                TimestampKeywordType pvValueTimestampKeywordType = null;
                try
                {
                    pvValueTimestampKeywordType = JsonConvert.DeserializeObject<TimestampKeywordType>(pvValueObj.ToString());
                }
                catch
                {
                    continue;
                }
                var timestampKeyword = pvValueTimestampKeywordType?.BaseTimestampKeyword;
                if (timestampKeyword == null) continue;
                try
                {
                    pvValueTimestampKeywordType.Validate();
                }
                catch (Exception exp)
                {
                    throw new Exception($"Error while computing value of PV '{keyProcessVariable}'. {exp.Message}");
                }
                TimestampKeywords.TimestampKeyword = pvValueTimestampKeywordType;
                var methodInfo = typeof(TimestampKeywords).GetMethod(timestampKeyword.Replace("[", "").Replace("]", ""));
                if (methodInfo == null) continue;
                var computedValue = methodInfo.Invoke(null, new object[] { });
                ProcessVariables[keyProcessVariable] = computedValue;
            }
        }

    }

}
