﻿using BH.ScalableServices.Workers.Plugins.Workflow.Shared.Models;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using Newtonsoft.Json.Linq;
using BH.ScalableServices.Workers.Plugins.Workflow.Shared.Interfaces;
using BH.ScalableServices.Workers.Plugins.Workflow.Tasks.Message.v1_0.Models;
using BH.ScalableServices.Workers.Shared.Helpers;
using Newtonsoft.Json;
using BH.ScalableServices.Brokers.SDKBase;
using Shared.WebClient.Models;
using BH.ScalableServices.Brokers.Shared.Models;
using Shared.WebClient;

namespace BH.ScalableServices.Workers.Plugins.Workflow.Tasks.Message.v1_0
{
    public class MessageRelayTask : WorkflowTask
    {
        
        public override IRunTaskReturn RunTask(JObject workingSet)
        {
            var messageTaskWorkingSet = workingSet.ToObject<MessageTaskWorkingSet>();
            Console.WriteLine($"Running Message Relay Task: {SystemTaskVariables.WorkflowTaskInstanceName}");

            var baseUri = Settings.MessageRelayBaseUrl;
            var creds = Settings.MessageRelayApiCreds;

            var vizApi = new ScalableServiceWebClient();
            vizApi.Setup(baseUri, creds);
            var bhMessages = JsonConvert.DeserializeObject<List<JToken>>(messageTaskWorkingSet.BHMessages);
            var customerName = messageTaskWorkingSet.CustomerName;
            var relayDlls = JsonConvert.DeserializeObject<List<string>>(messageTaskWorkingSet.RelayDllNames);
            var request= SetupMessageRelayApiRequest(bhMessages,customerName,relayDlls);
            var runDetails = JsonConvert.DeserializeObject<ScalableServiceRunDetails>(HttpHelper.HandleHttpResponse(HttpHelper.HandleRequest(request)));
            try
            {
                var runId = runDetails.RunId;
                LogEntryWriter.LogMessage($"RunId of the Message Relay Request: {runId}. Use this RunId to get the run details of the message relay request.", SystemTaskVariables.ProcessRunId);
                while (true)
                {
                    Thread.Sleep(1000);
                    runDetails = vizApi.GetDetails(runId,false);
                    if (runDetails == null || runDetails.Status == null)
                    {
                        continue;
                    }

                    if (runDetails.Status.IsException == true)
                    {
                        LogEntryWriter.LogMessage($"Status of the Message Relay Request : {JsonConvert.SerializeObject(runDetails.Status)}",SystemTaskVariables.ProcessRunId);

                        throw new Exception($"Message Relay request returned with 'is_exception=TRUE'," +
                             $" which caused the task to throw this exception. Use the RunId provided above to get the error details.");
                    }
                    
                    if (runDetails.Status.IsQueued == false && runDetails.Status.IsExecuting == false)
                    {
                        break;
                    }
                    Thread.Sleep(4000);
                }
            }
            catch (Exception exp)
            {
                LogEntryWriter.LogMessage(exp.Message, SystemTaskVariables.ProcessRunId);
                throw;
            }
            return new FinishRunTaskReturn()
            {
                WorkingSet = JObject.FromObject(messageTaskWorkingSet)
            };
        }

        private HttpRequest SetupMessageRelayApiRequest(List<JToken> bhMessages, string customerName, List<string> relayDlls)
        {
            var baseUri = Settings.MessageRelayBaseUrl;
            var uri = Settings.MessageRelayUri;
            var creds = Settings.MessageRelayApiCreds;

            foreach (var bhMessage in bhMessages)
            {
                ConvertUserBhMessageToTaskBhMessage(bhMessage, customerName);
                    
            }
            var body = CreateRelayMessage(bhMessages, relayDlls);
            var request = new HttpRequest(HttpVerb.Post) { BaseUri = baseUri };
            request.PathFragments.Add(new HttpPathFragment("uri", uri)); // URI to start
            if (creds != null)
                request.Headers.Add(new HttpHeader("Authorization", creds));
            request.Body = new StringContent(JsonConvert.SerializeObject(body));
            return request;
        }
        private dynamic CreateRelayMessage(List<JToken> bhMessages, List<string> relayDlls)
        {
            var relayMessage = new 
            {
                bh_messages = bhMessages,
                message_relay_dll_names = relayDlls 
            };
            return relayMessage;
        }
        private void ConvertUserBhMessageToTaskBhMessage(JToken bhMessage, string customerName)
        {
            // add customer name to the bhMessage
            bhMessage["customer_name"] = customerName;
        }
    }
}
