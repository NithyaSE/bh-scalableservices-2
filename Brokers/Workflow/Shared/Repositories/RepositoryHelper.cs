﻿using System.Collections.Generic;
using System.IO;
using SystemWrapper.IO;
using Newtonsoft.Json;
using BH.ScalableServices.Brokers.Workflow.Shared.Interfaces;
using BH.ScalableServices.Brokers.Workflow.Shared.Models;

namespace BH.ScalableServices.Brokers.Workflow.Shared.Respositories
{
    public static class RepositoryHelper
    {
        public static IWorkflowProcessInstance LoadProcessInstance(IFileWrap fileIO,string file)
        {
            var processInstanceJson = fileIO.ReadAllText(file);
            var processInstance = JsonConvert.DeserializeObject<WorkflowProcessInstance>(processInstanceJson);
            return processInstance;
        }

        public static void SaveProcessInstance(IFileWrap fileIO, string file, IWorkflowProcessInstance processInstance)
        {
            var processInstanceJSON = JsonConvert.SerializeObject(processInstance);
            fileIO.WriteAllText(file, processInstanceJSON);
        }

        public static IWorkflowTaskInstance LoadTaskInstance(IFileWrap fileIO, string file)
        {
            var taskInstanceJson = fileIO.ReadAllText(file);
            var taskInstance = JsonConvert.DeserializeObject<WorkflowTaskInstance>(taskInstanceJson);
            return taskInstance;
        }

        public static List<string> GetAllFilesUnderLocation(IDirectoryWrap dirIO ,string location)
        {
            var files = new List<string>();
            files.AddRange(dirIO.GetFiles(location));
            var dirs = dirIO.GetDirectories(location);
            foreach (var processDir in dirs)
            {
                files.AddRange(dirIO.GetFiles(processDir, "*", SearchOption.AllDirectories));

            }
            return files;
        }

    }
}
