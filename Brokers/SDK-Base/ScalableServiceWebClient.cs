﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net.Http;
using System.Threading;
using BH.ScalableServices.Brokers.Shared.Models;
using Shared.Tools;
using Shared.WebClient;
using Shared.WebClient.Models;
using Newtonsoft.Json;

namespace BH.ScalableServices.Brokers.SDKBase
{
    [Serializable]
    public class ScalableServiceWebClient : IScalableServiceWebClient
    {
        public string Endpoint { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public HttpHeader Header { get; set; }

        protected Mutex MultithreadControlMutex = new Mutex();
        public void Setup(string endpoint, string encryptedUserName, string encryptedPassword)
        {
            Endpoint = endpoint;
            Username = Encryption.DecryptString(encryptedUserName);
            Password = Encryption.DecryptString(encryptedPassword);
            Header = HttpHeader.EncodeBasicCredentials(Username, Password);
        }
        public void Setup(string endpoint, string credentials)
        {
            Endpoint = endpoint;
            Header = new HttpHeader("Authorization", credentials);
        }
        public void SetupRequest(HttpRequest request)
        {
            //only set these when Setup hasn't been applied or Null
            if (string.IsNullOrEmpty(Endpoint))
            {
                lock (MultithreadControlMutex)
                {
                    Endpoint = ConfigurationManager.AppSettings["ScalableApiEndPoint"];
                    Username = Encryption.DecryptString(ConfigurationManager.AppSettings["ScalableApiUsername"]);
                    Password = Encryption.DecryptString(ConfigurationManager.AppSettings["ScalableApiPassword"]);
                    Header = HttpHeader.EncodeBasicCredentials(Username, Password);
                }
            }
            request.BaseUri = Endpoint;
            request.Headers.Add(Header);
        }
        /// <summary>
        /// Provides a mechanism for a single entry to be recorded by the API for later returning via the \Details endpoint
        /// </summary>
        /// <param name="logEntry">The new logEntry to send to the API</param>
        /// <param name="runId"></param>
        /// <returns>An HttpResponseMessage for external checking of the results of the Post</returns>
        public  void PostLogEntry(ScalableServiceLogEntry logEntry,string runId)
        {
            var request = new HttpRequest(HttpVerb.Post);
            SetupRequest(request);
            request.HttpVerb = HttpVerb.Post;
            request.Body = new StringContent(JsonConvert.SerializeObject(logEntry));
            request.PathFragments.Add(new HttpPathFragment("endpoint", "LogEntries"));
            request.ParameterCollection.Add(new HttpParameter("run_id", runId));
            HttpHelper.HandleHttpResponse(HttpHelper.HandleRequest(request));
        }
        /// <summary>
        /// Provides a mechanism for array of LogEntry object(s) to be recorded by the API for later returning via the \Details endpoint
        /// </summary>
        /// <param name="logEntries">The new logEntry to send to the API</param>
        /// <param name="runId"></param>
        /// <returns>An HttpResponseMessage for external checking of the results of the Post</returns>
        public  void PostLogEntries(ICollection<ScalableServiceLogEntry> logEntries, string runId)
        {
            var request = new HttpRequest(HttpVerb.Post);
            SetupRequest(request);
            request.HttpVerb = HttpVerb.Post;
            request.Body = new StringContent(JsonConvert.SerializeObject(logEntries));
            request.PathFragments.Add(new HttpPathFragment("endpoint", "LogEntries"));
            request.ParameterCollection.Add(new HttpParameter("run_id", runId));
            HttpHelper.HandleHttpResponse(HttpHelper.HandleRequest(request));
        }
        /// <summary>
        /// Provides a mechanism for a single Status to be recoreded by the API for later returning via the \Details endpoint. 
        /// </summary>
        /// <param name="statusPatch"></param>
        /// <param name="runId"></param>
        /// <returns>An HttpResponseMessage for external checking of the results of the Update</returns>
        public  void UpdateStatus(object statusPatch,string runId)
        {
            var request = new HttpRequest(HttpVerb.Patch);
            SetupRequest(request);
            request.HttpVerb = HttpVerb.Patch;
            request.Body = new StringContent(JsonConvert.SerializeObject(statusPatch));
            request.PathFragments.Add(new HttpPathFragment("Status", "Status"));
            request.ParameterCollection.Add(new HttpParameter("run_id", runId));
            HttpHelper.HandleHttpResponse(HttpHelper.HandleRequest(request));
        }
        /// <summary>
        /// Gets the details of a run from the Server. 
        /// </summary>
        /// <param name="runId"></param>
        /// <returns>An HttpResponseMessage for external checking of the results of the Update</returns>
        public ScalableServiceRunDetails GetDetails(string runId,bool includeLogEntries=true)
        {
            if (runId == null)
                throw new Exception("The parameter runId can't be null.");
            var request = new HttpRequest(HttpVerb.Get);
            SetupRequest(request);
            request.PathFragments.Add(new HttpPathFragment("Details", "Details"));
            request.ParameterCollection.Add(new HttpParameter("run_id", runId));
            if(!includeLogEntries)
                request.ParameterCollection.Add(new HttpParameter("include_log_entries", includeLogEntries.ToString()));
            return JsonConvert.DeserializeObject<ScalableServiceRunDetails>(HttpHelper.HandleHttpResponse(HttpHelper.HandleRequest(request)));
        }
    }
}
