﻿using System;
using System.Net.Http;
using System.Text.RegularExpressions;
using BH.ScalableServices.Brokers.SDKBase;
using BH.ScalableServices.Brokers.Shared.Models;
using BH.ScalableServices.Workers.Plugins.Workflow.Shared.Interfaces;
using BH.ScalableServices.Workers.Plugins.Workflow.Shared.Models;
using BH.ScalableServices.Workers.Plugins.Workflow.Tasks.DocumentProduction.v2_2.Models;
using BH.ScalableServices.Workers.Shared.Helpers;
using Shared.WebClient;
using Shared.WebClient.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace BH.ScalableServices.Workers.Plugins.Workflow.Tasks.DocumentProduction.v2_2
{
    public class DocumentProduction : WorkflowTask
    {
        public override IRunTaskReturn RunTask(JObject workingSet)
        {
            Console.WriteLine($"Running Document Production Task: {SystemTaskVariables.WorkflowTaskInstanceName}");
            //Check if the system TV $last_check_timestamp exists on the task. yes-Then this is a rehydrated run, no - This is first run
            var docProdWorkingSet = workingSet.ToObject<DocProdWorkingSet>();
            var baseUri = Settings.DocumentProductionBaseUri;
            var uri = Settings.DocumentProductionUri;
            var creds = Settings.DocumentProductionCreds;
            var docProdAPI = new ScalableServiceWebClient();
            docProdAPI.Setup(baseUri, creds);
            if (!SystemTaskVariables.IsRehydrated)
            {
                //first run
                var checkInterval = docProdWorkingSet.CheckInterval;
                var regex = new Regex("^P(?!$)(\\d+Y)?(\\d+M)?(\\d+W)?(\\d+D)?(T(?=\\d+[HMS])(\\d+H)?(\\d+M)?(\\d+S)?)?$");
                var match = regex.Match(checkInterval);
                if (!match.Success)
                {
                    throw new Exception($"Invalid format of check interval {checkInterval}. Only ISO interval format accepted");
                }
                var body = new DocProductionBody
                {
                    template_file_path = docProdWorkingSet.TemplateFilePath,
                    plain_text_tag_mappings = docProdWorkingSet.PlainTextTagMappings,
                    images_path = docProdWorkingSet.ImagesPath,
                    content_masters = docProdWorkingSet.ContentMasters,
                    outputs = docProdWorkingSet.GeneratedFilePaths,
                    timeout = docProdWorkingSet.Timeout,
                    culture_info_code = docProdWorkingSet.CultureInfoCode
                };
                var request = SetupDocProdApiRequest(body);
                try
                {
                    var runDetails = JsonConvert.DeserializeObject<ScalableServiceRunDetails>(HttpHelper.HandleHttpResponse(HttpHelper.HandleRequest(request)));
                    var runId = runDetails.RunId;
                    docProdWorkingSet.DocProdRunId = runId;
                    LogEntryWriter.LogMessage($"RunId of the Document Production Request: {runId}. Use this RunId to get the run details of the document production request.", SystemTaskVariables.ProcessRunId);
                    return new DehydrateRunTaskReturn()
                    {
                        CheckInterval = docProdWorkingSet.CheckInterval,
                        WorkingSet = JObject.FromObject(docProdWorkingSet)
                    };
                }
                catch (Exception exp)
                {
                    LogEntryWriter.LogMessage(exp.Message, SystemTaskVariables.ProcessRunId);
                    throw;
                }
             }
            //rehydrated task
            LogEntryWriter.LogMessage($"Rehydrating task : {SystemTaskVariables.WorkflowTaskInstanceName}", SystemTaskVariables.ProcessRunId);
            var docProdRunId = docProdWorkingSet.DocProdRunId;
            if (!IsDocProdRunComplete(docProdAPI, docProdRunId))
            {
               return new DehydrateRunTaskReturn()
               {
                   CheckInterval  = docProdWorkingSet.CheckInterval,
                   WorkingSet = JObject.FromObject(docProdWorkingSet)
               };
            }
            return new FinishRunTaskReturn()
            {
                WorkingSet = JObject.FromObject(docProdWorkingSet)
            };
        }
       private bool IsDocProdRunComplete(ScalableServiceWebClient docProdAPI, string docProdRunId)
        {
            var runDetails = docProdAPI.GetDetails(docProdRunId,false);
            if (runDetails?.Status == null)
            {
                return false;
            }
            if (runDetails.Status.IsException == true)
            {
                LogEntryWriter.LogMessage($"Status of the Document Production Request : {JsonConvert.SerializeObject(runDetails.Status)}", SystemTaskVariables.ProcessRunId);
                throw new Exception($"Document production request returned with 'is_exception=TRUE'," +
                     $" which caused the task to throw this exception. Use the RunId provided above to get the error details.");
            }
            if (runDetails.Status.IsQueued != false || runDetails.Status.IsExecuting != false) return false;
            LogEntryWriter.LogMessage($"Document Production request complete. Check RunId : '{runDetails.RunId}' for details.", SystemTaskVariables.ProcessRunId);
            return true;
        }
        private HttpRequest SetupDocProdApiRequest(DocProductionBody body)
        {
            var baseUri = Settings.DocumentProductionBaseUri;
            var uri = Settings.DocumentProductionUri;
            var creds = Settings.DocumentProductionCreds;
            var request = new HttpRequest(HttpVerb.Post) { BaseUri = baseUri };
            request.PathFragments.Add(new HttpPathFragment("uri", uri)); // URI to start
            if (creds != null)
                request.Headers.Add(new HttpHeader("Authorization", creds));
            request.Body = new StringContent(JsonConvert.SerializeObject(body));
            return request;
        }
      }
}
