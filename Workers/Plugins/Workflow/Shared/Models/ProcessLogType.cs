﻿namespace BH.ScalableServices.Workers.Plugins.Workflow.Shared.Models
{
    public enum ProcessLogType
    {
        Info,
        Error,
        Warning
    }
}
