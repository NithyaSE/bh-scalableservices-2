﻿using System.Collections.Generic;

namespace BH.ScalableServices.Brokers.Workflow.Shared.Interfaces
{

    public interface ITaskInstanceRepository
    {
        void Add(string processInstanceName, IWorkflowTaskInstance taskInstance);

        void AddUpdate(string processInstanceName, string taskInstanceName, IWorkflowTaskInstance taskInstance);
        void Remove(string processInstanceName, string taskInstanceName);

        IWorkflowTaskInstance GetItem(string processInstanceName, string taskInstanceName);

        List<IWorkflowTaskInstance> GetItems(string processInstanceName);
    }
}
