﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using BH.ScalableServices.Brokers.Workflow.Handlers;
using BH.ScalableServices.Brokers.Workflow.Shared.Handlers;
using BH.ScalableServices.Brokers.Workflow.Shared.Interfaces;
using Nancy;
using Newtonsoft.Json;

namespace BH.ScalableServices.Brokers.Workflow.Endpoints
{
    /// <summary>
    /// 
    /// </summary>
    public class TaskSchemasEndpoints : ResponseHandlerNancyModule
    {
        private string routeVersion = "v2.0";

        public ITaskSchemaRepository TaskSchemaRepo { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public TaskSchemasEndpoints(ITaskSchemaRepository taskSchemaRepo)
        {
            TaskSchemaRepo = taskSchemaRepo;

            Get[routeVersion + "/taskSchemas"] = parameters =>
            {
                try
                {
                    var taskSchemaName = Request.Query["task_schema_name"].Value;
                    if (taskSchemaName != null)
                    {
                        var taskSchema = (ExpandoObject) GetSpecificTaskSchema(taskSchemaName);
                        return Response.AsJson(taskSchema).WithStatusCode(HttpStatusCode.OK);
                    }
                    var taskSchemas = GetAllTaskSchemas();
                    return Response.AsJson(taskSchemas).WithStatusCode(HttpStatusCode.OK);

                }
                catch (Exception ex)
                {
                    return HandleResponse(ex);
                }
            };

            Post[routeVersion + "/taskSchemas"] = parameters =>
            {
                try
                {
                    var taskSchemaName = Request.Query["task_schema_name"].Value;

                    string body;
                    using (var reader = new StreamReader(Request.Body))
                    {
                        body = reader.ReadToEnd();
                    }

                    var schema = JsonConvert.DeserializeObject<dynamic>(body);

                    if (taskSchemaName != null)
                    {
                        PostTaskSchema(taskSchemaName, schema);
                    }
                    else
                    {
                    //    if (schema != null && schema.GetType().GetProperty("name") != null)
                    //        PostTaskSchema(schema.name, schema);
                    //    else
                            throw new WorkflowException("Missing parameters task_schema_name.  Example: /taskSchemas?task_schema_name=[task_schema_name]", HttpStatusCode.BadRequest);
                    }
                    return HttpStatusCode.OK;

                }
                catch (Exception ex)
                {
                    return HandleResponse(ex);
                }
            };

            Delete[routeVersion + "/taskSchemas"] = parameters =>
            {
                try
                {
                    var taskSchemaName = Request.Query["task_schema_name"].Value;
                    if (taskSchemaName != null)
                    {
                        DeleteTaskSchema(taskSchemaName);
                        return HttpStatusCode.OK;
                    }
                    /*else
                    {
                        string body;
                        using (var reader = new StreamReader(Request.Body))
                        {
                            body = reader.ReadToEnd();
                        }

                        var schema = JsonConvert.DeserializeObject<dynamic>(body);

                        if (schema != null && schema.GetType().GetProperty("name") != null)
                            DeleteTaskSchema(schema.name);
                    }*/
                    throw new WorkflowException("Missing parameters task_schema_name.  Example: /taskSchemas?task_schema_name=[task_schema_name]", HttpStatusCode.BadRequest);

                }
                catch (Exception ex)
                {
                    return HandleResponse(ex);
                }
            };
        }



        private ExpandoObject GetSpecificTaskSchema(string taskSchemaName)
        {

            var taskSchema = (ExpandoObject)TaskSchemaRepo.GetItem(taskSchemaName);
            if (taskSchema == null)
            {
                throw new WorkflowException("Could not find the Task Schema :" + taskSchemaName, HttpStatusCode.NotFound);
            }


            return taskSchema;
        }

        private List<dynamic> GetAllTaskSchemas()
        {

            var taskSchemas = TaskSchemaRepo.GetItems();
            if (taskSchemas == null || !taskSchemas.Any() )
            {
                throw new WorkflowException("Could not find any Task Schemas.  Please make sure Task Schemas are installed.", HttpStatusCode.NotFound);
            }


            return taskSchemas;
        }

        private void PostTaskSchema(string taskSchemaName, dynamic schema)
        {
            TaskSchemaRepo.AddUpdate(taskSchemaName, schema);
        }

        private void DeleteTaskSchema(string taskSchemaName)
        {

            var taskSchema = (ExpandoObject)TaskSchemaRepo.GetItem(taskSchemaName);
            if (taskSchema == null)
            {
                throw new WorkflowException("Could not find the Task Schema :" + taskSchemaName, HttpStatusCode.NotFound);
            }
            else
            {
                TaskSchemaRepo.Remove(taskSchemaName);
            }

        }




    }

}
