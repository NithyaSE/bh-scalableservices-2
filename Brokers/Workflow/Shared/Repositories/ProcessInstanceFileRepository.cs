﻿using System.Collections.Generic;
using System.IO;
using System.Reflection;
using SystemWrapper.IO;
using Newtonsoft.Json;
using BH.ScalableServices.Brokers.Workflow.Shared.Interfaces;
using BH.ScalableServices.Brokers.Workflow.Shared.Models;

namespace BH.ScalableServices.Brokers.Workflow.Shared.Respositories
{
    public class ProcessInstanceFileRepository : IProcessInstanceRepository
    {
        #region Properties

        public IRelocator<IWorkflowProcessInstance> Relocator { get; set; }
        public IFileWrap FileIO { get; set; }

        public IDirectoryWrap DirIO { get; set; }

        public WorkflowSettings Settings { get; set; }

        private string _processInstanceFileName = "processinstance.json";
        #endregion

        #region Constructors
        public ProcessInstanceFileRepository(IRelocator<IWorkflowProcessInstance> relocator, IFileWrap fileWrap, IDirectoryWrap dirWrap, WorkflowSettings settings)
        {
            Relocator = relocator;
            FileIO = fileWrap;
            DirIO = dirWrap;
            Settings = settings;
            Settings.LoadSettings(Path.GetFileName(Assembly.GetEntryAssembly().CodeBase));
        }
        #endregion

        #region Public Repository Methods
        public void Add(IWorkflowProcessInstance processInstance)
        {
            var processPath = Path.Combine(Settings.ProcessBundlesLibraryLocation, processInstance.Name);
            var processInstancePath = Path.Combine(processPath, _processInstanceFileName);

            if (!DirIO.Exists(processPath))
                DirIO.CreateDirectory(processPath);

            var processInstanceJson = JsonConvert.SerializeObject(processInstance);
            FileIO.WriteAllText(processInstancePath, processInstanceJson);
        }
        public void AddUpdate(string processInstanceName, IWorkflowProcessInstance processInstance)
        {
            if (processInstanceName != processInstance.Name)
            {
                Relocator.Relocate<WorkflowProcessInstance>(processInstanceName, processInstance.Name);
            }
            Add(processInstance);
        }
        public void Remove(string name)
        {
            var processPath = Path.Combine(Settings.ProcessBundlesLibraryLocation, name);
            if (DirIO.Exists(processPath))
                DirIO.Delete(processPath, true);
        }
        public IWorkflowProcessInstance GetItem(string processName)
        {
            var processInstance = LoadProcessInstanceFromName(processName);
            return processInstance;
        }
        public string GetItemAsJson(string processName)
        {
            var processInstance = GetItem(processName);
            return JsonConvert.SerializeObject(processInstance); ;
        }
        public List<IWorkflowProcessInstance> GetItems()
        {
            var processInstances = new List<IWorkflowProcessInstance>();

            var files = RepositoryHelper.GetAllFilesUnderLocation(DirIO, Settings.ProcessBundlesLibraryLocation);
            foreach (var file in files)
            {
                var filename = Path.GetFileName(file ?? "").ToLower();
                if (!filename.StartsWith("processinstance") || !filename.EndsWith(".json")) continue;
                if (file == null) continue;
                var processInstance = RepositoryHelper.LoadProcessInstance(FileIO, file);

                processInstances.Add(processInstance);
            }
            return processInstances;
        }
        #endregion
        #region Private Helper Methods
        private IWorkflowProcessInstance LoadProcessInstanceFromName(string processName)
        {
            var files = RepositoryHelper.GetAllFilesUnderLocation(DirIO, Settings.ProcessBundlesLibraryLocation);

            foreach (var file in files)
            {
                var directory = new DirectoryInfo(Path.GetDirectoryName(file) ?? "").Name;

                var filename = Path.GetFileName(file ?? "").ToLower();

                if (directory != processName || !filename.StartsWith("processinstance") ||
                    !filename.EndsWith(".json")) continue;
                if (file == null) continue;
                var processInstance = RepositoryHelper.LoadProcessInstance(FileIO, file);
                return processInstance;
            }
            return null;
        }
        public IWorkflowProcessInstance GetItemAtLocation(string processBundleLoc)
        {
            var files = RepositoryHelper.GetAllFilesUnderLocation(DirIO, processBundleLoc);

            foreach (var file in files)
            {
                var filename = Path.GetFileName(file ?? "").ToLower();

                if (!filename.StartsWith("processinstance") || !filename.EndsWith(".json")) continue;
                if (file == null) continue;
                var processInstance = RepositoryHelper.LoadProcessInstance(FileIO, file);
                return processInstance;
            }
            return null;
        }
        #endregion
     }
}