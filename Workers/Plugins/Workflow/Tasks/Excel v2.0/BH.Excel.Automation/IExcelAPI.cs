﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BH.Excel.Automation
{
    public interface IExcelAPI : IDisposable
    {
        Scripting.Dictionary CallExcelMacroWithDictionary(string filename, string macroName,
            Scripting.Dictionary parameters, int numberOfAttempts = 20, bool save = true, int timeoutSeconds = 900);

        void SetIsVisible(bool isVisible);
        void TryKillingExcelProcess();
    }
}
