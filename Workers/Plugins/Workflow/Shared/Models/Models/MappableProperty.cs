﻿using Newtonsoft.Json;

namespace BHX.Workflow.Shared.Models.Models
{
    public class MappableProperty<T>
    {
        [JsonProperty("is_mapped")]
        public bool isMapped { get; set; }

        [JsonProperty("process_variable")]
        public string ProcessVariable { get; set; }

        [JsonProperty("value")]
        public T Value { get; set; }
        
    }
}
