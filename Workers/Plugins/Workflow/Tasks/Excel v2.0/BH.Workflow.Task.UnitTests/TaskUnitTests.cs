﻿using System.IO;
using BH.Excel.Automation;
using BH.Workflow.Task.ExcelWithParameters;
using BH.Workflow.TestHarness;
using BHX.Workflow.Shared.Models.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;

namespace BH.Workflow.Task.UnitTests
{
    [TestClass]
    public class TaskUnitTests
    {
        [TestMethod]
        
        public void Check_Excel_Task()
        {
            var mockJsonInstance = File.ReadAllText("MockTaskInstances\\mocktaskinstance1.json");

            var fakeExcelAPI = new FakeExcelAPI();

            var task = new ExcelWithParamsTask(fakeExcelAPI)
            {
                BloodhoundTaskInstance = JsonConvert.DeserializeObject<BloodhoundTaskInstance>(mockJsonInstance)
            };

            var taskTest = new WorkflowTaskTester(task);
            taskTest.RunTask();

            var isVisible = fakeExcelAPI.IsVisible;

            Assert.IsTrue(isVisible);
        }


        [TestMethod]

        public void Check_Excel_Task_Will_Not_Save()
        {
            var mockJsonInstance = File.ReadAllText("MockTaskInstances\\mocktaskinstance1.json");

            var fakeExcelAPI = new FakeExcelAPI();

            var task = new ExcelWithParamsTask(fakeExcelAPI)
            {
                BloodhoundTaskInstance = JsonConvert.DeserializeObject<BloodhoundTaskInstance>(mockJsonInstance)
            };

            var taskTest = new WorkflowTaskTester(task);
            taskTest.RunTask();

            var save = fakeExcelAPI.Save;

            Assert.IsTrue(save == false);
        }
        

    }




}
