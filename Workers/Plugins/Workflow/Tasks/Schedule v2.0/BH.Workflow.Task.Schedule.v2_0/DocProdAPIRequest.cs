﻿using System.Net.Http;
using BH.ScalableServices.Workers.Plugins.Workflow.Models;
using Shared.WebClient;
using Shared.WebClient.Models;

namespace BH.ScalableServices.Workers.Plugins.Workflow.Tasks.DocumentProduction.v2_2
{
    public class DocProdAPIRequest : IApiRequest
    {
        public object Request { get; set; }
        public HttpResponseMessage Response { get; set; }

        public HttpResponseMessage HandleRequest()
        {
            var response = HttpHelper.HandleRequest((HttpRequest)Request);
            Response = response;
            return response;
        }
    }
}
